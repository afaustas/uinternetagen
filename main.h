#ifndef __MAIN_H
#define __MAIN_H

#define LONGEST_ALLOWED_SITE_NAME_LEN       200

#define _WIN32_WINNT 0x0501
#include <winsock2.h>
//#include <Ws2tcpip.h>
#include <windows.h>
#include <windowsx.h>
#include <stdio.h>
#include <string>
#include <ostream>
#include <stdlib.h>
#include <time.h>

#include "boolean.h"

#include "types.h"
#include "WebSite.h"
#include "YouTube.h"
#include "Google.h"
#include "EmailClient.h"

#include <afxres.h>
#include "resources.h"

#include <Shlwapi.h>

#define DEFAULT_CLIENT_STRING   "Google Chrome : Mozilla/5.0 (Windows NT 6.1), AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36"
#define MAX_CLIENT_STRING_LEN   256

#define MAX_EMAIL_ADDRESS_LENGTH    50
#define MAX_EMAIL_SUBJECT_LENGTH    256
#define MAX_EMAIL_MESSAGE_LENGTH    8192
#define MAX_USERNAME_LENGTH         64
#define MAX_PASSWORD_LENGTH         64

#include <streambuf>
#include <vector>
#include <cassert>

#include "logger.h"

#endif // __MAIN_H
