#include "logger.h"

logger log;

void logger::setFileName(string fileName){
    fileName_ = fileName;
    if(fileName != ""){
        file.open(fileName.c_str(), std::ofstream::out | std::ofstream::app);
        if(file.good()){
            isFileLoggingOn = true;
        }
        else{
            //TODO: pranesimas turetu buti kraunamas is resurso.
            MessageBoxA(NULL, "Nepavyksta sukurti failo rasymui/rasyti i faila. Galbut blogas failo vardas.", "Klaida", MB_OK | MB_ICONERROR);
        }
    }
}

void logger::stopFileLogging(){
    isFileLoggingOn = false;
    file.close();
    fileName_ = "";
}

void logger::getDate(){
    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime (dateBuffer,80,"\r\n[%Y-%m-%d %H:%M:%S] ",timeinfo);
}

void logger::printString(const string &s){
    string logLine;
    unsigned long int ndx;

    if(insertDateTime == true) {
        getDate();
        logLine = dateBuffer;
        insertDateTime = false;
    }
    logLine += s;

    ndx = GetWindowTextLength (hWnd_);
    SendMessage(hWnd_, EM_SETSEL, ndx, ndx);
    SendMessage(hWnd_, EM_REPLACESEL, TRUE, (LPARAM)logLine.c_str());

    if(fileName_ != ""){
        file << logLine;
        file.flush();
    }
}

logger & operator<<(logger &l, const string &s){
    l.printString(s);
    return l;
}

logger & operator<<(logger &l, const int &i){
    char integer[10];
    itoa(i, integer, 10);
    l.printString(integer);
    return l;
}

//logger & operator<<(logger &o, logger &o_){
logger & operator<<(logger &l, logger &(&fptr)(logger &o)){
    printf("|\r\n");
    l.insertDateTime = true;
    return l;
}

logger & endLogRec(logger &l){
    l.insertDateTime = true;
    return l;
}

void writeLogDataLine(LOGDATA *logData, fstream *TSVfile, bool linesCountReset){
    time_t rawtime;
    struct tm * timeinfo;
    char dateBuffer[80];
    extern fstream g_TSV_file;
    static int linesCount;

    if(linesCountReset){
        linesCount = 0;
        log << "       My  IP    |   Server    IP  | My Port | Srv Port | Bytes Sent | Bytes Recv | Link" << endLogRec;
    }
    else{
        if(linesCount == 0){
            log << "       My  IP    |   Server    IP  | My Port | Srv Port | Bytes Sent | Bytes Recv | Link" << endLogRec;
        }
        else if (linesCount == LOG_LINES_PER_PAGE){
            linesCount = 0;
        }
        linesCount++;

        log << (int)logData->myIP[0] << "." << (int)logData->myIP[1] << "." << (int)logData->myIP[2] << "." << (int)logData->myIP[3] << " | ";
        log << (int)logData->srvIP[0] << "." << (int)logData->srvIP[1] << "." << (int)logData->srvIP[2] << "." << (int)logData->srvIP[3] << " | ";
        log << logData->myPort << " | " << logData->srvPort << " | " << logData->bytesSent << " | " << logData->bytesRecv << " | " << logData->link << endLogRec;

        if(TSVfile != NULL){
            if(TSVfile->good()){
                time (&rawtime);
                timeinfo = localtime (&rawtime);

                strftime (dateBuffer,80,"%Y-%m-%d",timeinfo);
                *TSVfile << dateBuffer << '\t';
                strftime (dateBuffer,80,"%H:%M:%S",timeinfo);
                *TSVfile << dateBuffer << '\t';
                *TSVfile << (unsigned long int)rawtime << '\t';
                *TSVfile << (int)logData->myIP[0] << "." << (int)logData->myIP[1] << "." << (int)logData->myIP[2] << "." << (int)logData->myIP[3] << '\t';
                *TSVfile << (int)logData->srvIP[0] << "." << (int)logData->srvIP[1] << "." << (int)logData->srvIP[2] << "." << (int)logData->srvIP[3] << '\t';
                *TSVfile << logData->myPort << '\t' << logData->srvPort << '\t';
                *TSVfile << logData->bytesSent << '\t' << logData->bytesRecv << '\t' << logData->link << endl;
                TSVfile->flush();
            }
        }
    }
}
