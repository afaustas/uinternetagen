#include "socket_level_communication.h"

extern logger log;

void SocketCommunication::EndConnection(){
    ///BIO_free_all(bio);  //--- kazko luzta
    if(bio != NULL){
        BIO_free_all(bio);
        bio = NULL;
        log << "CONNECTION CLOSED!" << endLogRec;
    }
    if(isSecureConnection){
        // Remember, we also need to free up that SSL_CTX object!
    //    SSL_CTX_free(ctx);
    }
}

//Site yra tik domeno vardas su galimu portu. Pvz.: "www.afaustas.lt:100", o isSecure nurodo ar rysys saugus
BOOL SocketCommunication::SetConnection(char Site[], bool isSecure){
   // Create a SSL object pointer, which our BIO object will provide.
   SSL* ssl;

   isSecureConnection = isSecure;
   if(isSecure){
        ctx = SSL_CTX_new(SSLv23_client_method());
   }

   if(strchr(Site, ':')){ //Jei portas nurodytas
        if(isSecure){
            bio = BIO_new_ssl_connect(ctx);
            // Makes ssl point to bio's SSL object.
            BIO_get_ssl(bio, &ssl);
            // Set the SSL to automatically retry on failure.
            SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
            // We're connection to google.com on port 443.
            BIO_set_conn_hostname(bio, Site);
        }
        else{
            bio = BIO_new_connect(Site);
        }
        connectedTo = Site;
   }
   else{ //Jei portas nenurodytas, tai naudojamas standartinis (http 80 arba https 443)
        string str(Site); //Laikinas stringas

        if(isSecure){
             str += ":443";
             bio = BIO_new_ssl_connect(ctx);
             // Makes ssl point to bio's SSL object.
             BIO_get_ssl(bio, &ssl);
             // Set the SSL to automatically retry on failure.
             SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
             // We're connection to google.com on port 443.
             BIO_set_conn_hostname(bio, str.c_str());
        }
        else{
             str += ":80";
             bio = BIO_new_connect((char*)str.c_str());
        }

        connectedTo = str;
   }

   // Was the BIO successfully created?
   if (bio == NULL) {
      log << "socket_level_communication.cpp: Error creating BIO!" << endLogRec;
      // This function prints out error messages. Use Google and the OpenSSL documentation to find out what the problem is.
      ERR_print_errors_fp(stderr);
      connectedTo = "";
      if (isSecure) SSL_CTX_free(ctx);
      return FALSE;
   }

   // BIO_do_connect() tells our BIO object to connect to the server.
   if (BIO_do_connect(bio) <= 0) {
      log << "socket_level_communication.cpp: Failed to connect!" << endLogRec;
      connectedTo = "";
      BIO_free_all(bio); ///0820 PRERELEASE atkomentuota 0827
      bio = NULL; //Prideta 0827
      if (isSecure) SSL_CTX_free(ctx);
      return FALSE;
   }

   #ifdef DEBUG
   log << "socket_level_communication.cpp: Connected successfully!!!" << endLogRec;
   #endif

   return TRUE;
}

BOOL SocketCommunication::reconnect(){
    EndConnection();
	return SetConnection((char*)connectedTo.c_str(), isSecureConnection);
}

char * SocketCommunication::get_answer(unsigned int *received){
	unsigned int current_recv, recv_stat;
	char *olddata = NULL;
	char tmp[1430];
	bool first_iterration = true;
	unsigned long int content_length = 0;
printf("a");
	num_bytes_received = 0;

    /* Jeigu buvo rezervuota atmintis, tai viska istrinti. */
    free(data);
    data = NULL;
printf("b");
   // Loop while there's information to be read.
   do {
      // Create a buffer for grabbing information from the page.
      char tmp[1024];printf("c");
      memset(tmp, 0, sizeof(tmp));
printf("p");
      // BIO_read() reads data from the server into a buffer. It returns the number of characters read in.
      current_recv = BIO_read(bio, tmp, sizeof(tmp) - 1);
      #ifdef DEBUG
      //log << "current_recv=" << current_recv << endLogRec;
      #endif
      if(current_recv == -1) current_recv = 0; //Is esmes -1 ir 0 yra tas pats ir is tiesu nera klaida, o tik pranesimas, kad siuo metu nera ka nuskaityti.

      if(first_iterration){
         char *cl_start, *head_end;
         char tmpchr = tmp[sizeof(tmp) - 1];
         tmp[sizeof(tmp) - 1] = '\0';
         #ifdef DEBUG
         log << "tmp = " << tmp << endLogRec;
         #endif
         cl_start = strstr(tmp, "Content-Length: ");
         if(cl_start){
            cl_start += sizeof("Content-Length: ") - 1;
            //cl_end1 = strchr(cl_start, '\r');
            //cl_end2 = strchr(cl_start, '\n');
            //if(cl_end1) cl_end1[0] = '\0';
            //if(cl_end2) cl_end2[0] = '\0';
            content_length = atol(cl_start);
            if((head_end = strstr(tmp, "\r\n\r\n")) == NULL){
                if((head_end = strstr(tmp, "\n\r\n\r")) == NULL){
                    /*if((head_end = strstr(tmp, "\r\r")) == NULL){  -- Siaip jau nemaciau, kad taip butu
                        head_end = strstr(tmp, "\n\n");
                    } */
                }
            }
            content_length += (head_end + 4 - tmp);  //^Del to cia ne 2, o 4.
            #ifdef DEBUG
            log << "Gavosi: " << cl_start << "Istraukiau: " << content_length << endLogRec;
            #endif
            //if(cl_end1) cl_end1[0] = '\r';
            //if(cl_end2) cl_end2[0] = '\n';
         }
         tmp[sizeof(tmp) - 1] = tmpchr;
         first_iterration = false;
      }

      #ifdef DEBUG
      //log << "Po tmp priemimo current_recv = " << current_recv << endLogRec;
      #endif
      // If we haven't read in anything, assume there's nothing more to be sent since we used Connection: Close.

      // If BIO_read() returns a negative number, there was an error
      if (current_recv < 0) {
         // BIO_should_retry lets us know if we should keep trying to read data or not.
         if (!BIO_should_retry(bio)) {
            log << "socket_level_communication.cpp: Read Failed!" << endLogRec;
            BIO_free_all(bio);
            bio = NULL;
            free(data);
            return NULL;
         }
      }
      // We actually got some data, without errors!
      else if(current_recv) {
         // Null-terminate our buffer, just in case

 		 olddata = data;
 		 #ifdef DEBUG
 		 ///log << "Reikalauju atminties: " << num_bytes_received + current_recv + 1 << endLogRec;
 		 #endif
		 data = (char*)realloc(data, num_bytes_received + current_recv + 1); //+1 rezervui. Paskutineje iteracijoje labai svarbu, kad butu prideta, tam kad null terminatorius neiseitu z ribu.
		 if (data == NULL){
			data = olddata; //Paliekami seni duomenys
            log << "socket_level_communication.cpp: ERROR: Out of memory!!!" << endLogRec;
			data[num_bytes_received - 1] = 0;
			return NULL;
		 }

         memcpy(&data[num_bytes_received], tmp, current_recv);
		 num_bytes_received += current_recv;
	  }
   }
   while(((current_recv != 0) && (num_bytes_received < content_length))  || ((content_length == 0) && current_recv != 0) );
   //    ^--Jei headeryje dydis nurodytas                                   ^--Jei headeryje dydis nenurodytas, tai apie pabaiga sprendziu is paketo, kurio dydis yra 0.

    data[num_bytes_received] = 0;

    // Echo what the server sent to the screen
    //printf("->>> %s \n", data);

	log << "Downloaded" << num_bytes_received << "bytes.\n" << endLogRec;

    if(data){
        this->answer = data;
        this->LengthOfAnswer = num_bytes_received;
    }

    return data;
}

int SocketCommunication::getAnswerFragment(char *buffer, unsigned int bufferLength){
    int current_recv, recv_stat, retry = 0;
	char *data = NULL, *olddata = NULL;
	char tmp[1430];
	int num_bytes_received;

    num_bytes_received = BIO_read(bio, buffer, bufferLength);
    //log << "num_bytes_received=" << num_bytes_received << endLogRec;

//TODO: CIA NUMATYTI KLAIDAS GALIMA, BET TIK BE WINAPI(!)

	return num_bytes_received;
}

unsigned long int SocketCommunication::httpQuery(SOCKET &sck, char *query/*, char &answer[]*/){
    unsigned int bytes_received = 0;

   /* Siunciu uzklausa */
   // BIO_puts sends a null-terminated string to the server. In this case it's our GET request.
   BIO_puts(bio, query);
    /* Priimu atsakyma  */
    {
/*        int rtry = 0;
        char *data = NULL;
        //data = (char *)0x274C;
        bytes_received = 0xFFFF;
        for (rtry = 0; (rtry < 10) && (data == NULL) && (bytes_received == 0xFFFF); rtry++){
            if (rtry)
                printf("Retrying... \n"); */
                printf("0");

        data = NULL; //Atlieka get_answer()
        free(data); //Atlieka get_answer()
            printf("1");
            data = get_answer(&bytes_received);
/*            ///break;
        }   */
    }
    return bytes_received;
}

unsigned int SocketCommunication::GetIPandPort(unsigned char ip[], bool needMyData){
    SOCKET sock_fd;
    IN_ADDR a;
    unsigned int port = 0;

    ip[0] = '\0';
    ip[1] = '\0';
    ip[2] = '\0';
    ip[3] = '\0';

    if(BIO_get_fd(bio, &sock_fd) != -1){
        struct sockaddr_in s_in;
        //union38_S_un s;
        int s_in_len = sizeof(s_in);

        if(needMyData){
            getsockname(sock_fd, (struct sockaddr*)&s_in, &s_in_len);
        }
        else{
            getpeername(sock_fd, (struct sockaddr*)&s_in, &s_in_len);
        }

        if (s_in.sin_family == AF_INET){
            ip[0] = s_in.sin_addr.S_un.S_un_b.s_b1;
            ip[1] = s_in.sin_addr.S_un.S_un_b.s_b2;
            ip[2] = s_in.sin_addr.S_un.S_un_b.s_b3;
            ip[3] = s_in.sin_addr.S_un.S_un_b.s_b4;
            port = ntohs(s_in.sin_port);
        }
        else if (s_in.sin_family == AF_INET6){
            //IN6_ADDR sin6_addr = (IN6_ADDR)s_in.sin_addr;
            log << "Nesitikejau, kad gali buti IPv6, bet nesunku sutvarkyt" << endLogRec;
        }
        else
          log << "Nezinomas socketo tipas: " << s_in.sin_family << endLogRec;
    }
    return port;
}
