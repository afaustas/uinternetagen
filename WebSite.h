#ifndef __WEBSITE_H
#define __WEBSITE_H

using namespace std;

#include "socket_level_communication.h"
#include "logger.h"
#include "types.h"

#include <string>
#include <vector>
#include <algorithm>

#include <iostream>
#include <fstream> //Laikinai
#include <windows.h>

#define MAX_LINKS               1024
#define MAX_LINK_LENGTH         8192
#define MAX_DOMAIN_NAME_LENGTH  128
#define MAX_ATTRIB_VAL          30
#define MAX_PROTOCOL_NAME_LENGTH 10
#define MAX_CACHED_FILES         5

#define MAX_STACK    (1*1024*1024UL)    /* 1 MB - leidziamas didziausias steko dydis del rekursijos fooDownload(). Windows OS pagal default max 1 MB duodamas procesui */

class WebSite{
public:
    WebSite();
    WebSite(char SiteName[]){SetDefaultUserAgent(); SetName(SiteName);WebSite();};
    ~WebSite();
    void SetDefaultUserAgent(){UserAgent="Mozilla/5.0 (Windows NT 6.1), AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";};
    void SetUserAgent(string UserAgent_){UserAgent = UserAgent_;};
    bool SetName(char SiteName[]); ///galvoti apie sekme
    bool httpQuery(char query[]);
    char *GetAnswer(){return sc.answer;};
    bool GetListOfLinks();
    //Browse();
    //BrowseForever();
    unsigned long int TagParser(const char htmlTag[], const char htmlAttrib[], char htmlValue[],
                                const unsigned int maxHtmlValueLen, const char data[],
                                const unsigned long int dataLength, const unsigned long int curPosition);
    //bool LoadPage(const char link[]);
    bool LoadPage(const char link[], char *newPath, int newPathLength);

    const char *extractFileName(const char link[]);
    string extractDirectoryName(const char link[]);
    bool extractPathName(const char link[], char *pathName, int maxPathLength);
    bool extractDomainName(const char link[], char *domainName, int maxDomainLength);
    bool extractProtocolName(const char link[], char *protocolName, int maxProtocolNameLength);
    unsigned long int extractContentLength(char *httpAnswerHeader);

    //void fooLoadFiles(char currentDomainName[], char page[], unsigned long int pageLength, char tag[], char attribute[]);
    void fooLoadFiles(char currentPath[], char page[], unsigned long int pageLength, char tag[], char attribute[], char *whenAttribute = NULL, char *whenValue = NULL);
    void fooDownload(char link[]);

    ///void Browse(char webSiteName[], unsigned long int timeMin, unsigned long int timeMax);
    void Browse(const string webSiteName, unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax);
    void Browse(unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax);
unsigned long int LinksInList = 0;
///char linkList[MAX_LINKS][MAX_LINK_LENGTH];
    char **linkList; //Norint didelio saraso reikia sukurti heape, tai atliekama konstruktoriuje.
    string getCurrentSiteName(){return CurrentSiteName;};
private:
    string Name, CurrentSiteName; //CurrentSiteName dar savyje saugo protokola
    SocketCommunication sc;
//    Connect();
    struct sockaddr_in s_in; ///TODO: DINAMINIS REZERVAVIMAS! global?
    SOCKET sck;
  //  char *answer = NULL;
    string UserAgent;
  //  string originalName;
    bool isSecureConnection = false;
    LOGDATA logData;
    void removeNewLinesAndChunkData(char *link);
};

int64_t GetTimeMs64();

#endif // __WEBSITE_H
