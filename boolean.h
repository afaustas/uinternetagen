#ifndef __BOOLEAN_H
#define __BOOLEAN_H

typedef int BOOL;

#define TRUE    1
#define FALSE   0

#endif // __BOOLEAN_H
