#ifndef __SOCKET_LEVEL_COMMUNICATION_H
#define __SOCKET_LEVEL_COMMUNICATION_H

#define WIN32_LEAN_AND_MEAN
#define MSG_WAITALL 0x08
//#define LONGEST_ALLOWED_SITE_NAME_LEN       200

#define RECV_RETRY      3
#define RECONNECT_RETRY 3

#include <winsock2.h>
//#include <ws2tcpip.h>
#include <windows.h>
//#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
//#include <alloc.h>
#include <string>

#include "boolean.h"


#include <openssl/bio.h> // BIO objects for I/O
#include <openssl/ssl.h> // SSL and SSL_CTX for SSL connections
#include <openssl/err.h> // Error reporting
//#include <winsock2.h>

#include <iostream>

#include "logger.h"

using namespace std;

class SocketCommunication{
public:
    BOOL SetConnection(char Site[], bool isSecure = false);
    void EndConnection();
    unsigned long int httpQuery(SOCKET &sck, char *query/*, char answer[]*/);  //Grazina ilgi baitais
    char *answer;
    unsigned long int GetLengthOfAnswer(){return LengthOfAnswer;}
    //~SocketCommunication();
    BOOL reconnect();
    int getAnswerFragment(char *buffer, unsigned int bufferLength);

    unsigned int GetIPandPort(unsigned char ip[], bool needMyData);

    BIO* bio = NULL;
    SSL_CTX *ctx;
private:


    char *get_answer(unsigned int *received);
    unsigned long int LengthOfAnswer;

    char *data = NULL;
    string connectedTo;
    int num_bytes_received;
    bool isSecureConnection;
};

#endif
