#include "Google.h"

bool Google::LoadMainPage(){
    if(!ws.LoadPage("https://www.google.lt", newPath, MAX_LINK_LENGTH)){
       return false;
    }
    return true;
}

void Google::search(string keywords, unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax){
    string googleQuery("https://www.google.lt/search?q=");
    googleQuery += keywords;
    ws.Browse(googleQuery, depthMin, depthMax, timeMin, timeMax);
}
