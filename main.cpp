#include "main.h"

//#include <openssl/applink.c> // To prevent crashing (see the OpenSSL FAQ)
#include <openssl/applink.c> // To prevent crashing (see the OpenSSL FAQ)
#include <openssl/bio.h> // BIO objects for I/O
#include <openssl/ssl.h> // SSL and SSL_CTX for SSL connections
#include <openssl/err.h> // Error reporting

#define _WIN32_IE 0x0300
#define CONFIG_FILE_NAME    "./ktrsg.ini"
#include <Commctrl.h>

#define MAX_KEYWORD_LENGTH 256
#define MAX_MESSAGE_STRING_LENGTH 2048
#define MAX_SHORT_MESSAGE_STRING_LENGTH 128

#define VERSION "1.0"
#define DATE "2014-08-30"

///TAISYTI 20140901:
///1. Ivedimas http:// tik
///2. Tuscios eilutes kelia http:// (turbut automatiskai issispres issprendus (1).
///3. Tuscios eilutes Google - atmesti
///4. \r\n pasalint is tsv linku
///5. Memory leaks!

using namespace std;

typedef struct _webSiteData{
    char **webSiteList;
    bool isRandomOrder = true;
    unsigned int repeatMin = 1;
    unsigned int repeatMax = 1;
    unsigned int depthMin = 1;
    unsigned int depthMax = 4;
    unsigned long int lookMin = 2000;
    unsigned long int lookMax = 4000;
    unsigned int totalWebSites = 0;
} WEBSITEDATA;

typedef struct _googleData{
    char **keywordsList;
    bool isRandomOrder = true;
    unsigned int repeatMin = 1;
    unsigned int repeatMax = 1;
    unsigned int depthMin = 1;
    unsigned int depthMax = 4;
    unsigned long int lookMin = 2000;
    unsigned long int lookMax = 4000;
    unsigned int totalKeywords = 0;
} GOOGLEDATA;

typedef struct _cclient{
    bool isOn = false;
    char serverName[MAX_DOMAIN_NAME_LENGTH];
    unsigned long int queryLengthMin = 10;
    unsigned long int queryLengthMax = 100;
    unsigned long int queryPeriodMin = 500;
    unsigned long int queryPeriodMax = 1500;
} CCLIENT;

typedef struct _cserver{
    bool isOn = false;
    unsigned int port;
    unsigned long int answerLengthMin = 1024;
    unsigned long int answerLengthMax = 8096;
} CSERVER;

typedef struct _custom_server_client{
    bool isTCP = true;
    CCLIENT cclient;
    CSERVER cserver;
} CUSTOM_SERVER_CLIENT;

typedef struct _email_settings{
    char server[MAX_DOMAIN_NAME_LENGTH];
    unsigned int port;
    bool useSSL = true;
    bool useAuth = true;
    char userName[MAX_USERNAME_LENGTH];
    char Password[MAX_PASSWORD_LENGTH];
} EMAIL_SETTINGS;

typedef struct _email{
    EMAIL_SETTINGS imapSettings;
    EMAIL_SETTINGS smtpSettings;
    bool isSendOn = false;
    bool isRecvOn = false;
    char From[MAX_EMAIL_ADDRESS_LENGTH];
    char To[MAX_EMAIL_ADDRESS_LENGTH];
    char Subject[MAX_EMAIL_SUBJECT_LENGTH];
    char Message[MAX_EMAIL_MESSAGE_LENGTH];
    unsigned int prepTimeMin = 5;
    unsigned int prepTimeMax = 10;
    unsigned int listTimeMin = 5;
    unsigned int listTimeMax = 10;
    unsigned int viewTimeMin = 5;
    unsigned int viewTimeMax = 10;
} EMAIL;

typedef struct _formData{
    WEBSITEDATA webSiteData;
    GOOGLEDATA googleData;
    CUSTOM_SERVER_CLIENT customServerAndClient;
    EMAIL email;
    bool watchYoutube = true;
    unsigned int youtubeRepeatMin = 1;
    unsigned int youtubeRepeatMax = 1;
    bool useGoogle = true;
    bool useLinks = true;
    bool isLoggingOn = true;
    char fileName[MAX_PATH]; ///Ar cia mano tas MAX_PATH???
    char TSVfileName[MAX_PATH];
    char *clientString = NULL;
    bool isRandom = true;
    bool isTSV_on = false;
} FORMDATA;

BOOL CALLBACK MainDialogProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK EmailSettingsDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);

DWORD WINAPI Thread_Main(LPVOID pvHDlg);

HANDLE HThread_Main = 0;

FORMDATA formData;

void onStart(HWND hDlg);
void onInit(HWND hDlg);
void loadConfiguration(HWND hDlg);
void refreshConfigurationStruct(HWND hDlg);
void saveConfiguration(HWND hDlg);
bool inputCheck(HWND hDlg);
void initEmailSettingsDialog(HWND hDlg);
void writeLogDataLine(LOGDATA &logData, bool linesCountReset = false);

void browseWebsite(bool &started);
void searchGoogle(bool &started);
void warchYoutube(bool &started);

bool gStarted = false;
bool gStopped = true;

extern logger log;
fstream g_TSV_file;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    HWND hMainDlg;
    BOOL ret;
    MSG msg;

    srand (time(NULL));

    /* OpenSSL bibliotekai initializuoti */
    CRYPTO_malloc_init();           // Initialize malloc, free, etc for OpenSSL's use
    SSL_library_init();             // Initialize OpenSSL's SSL libraries
    SSL_load_error_strings();       // Load SSL error strings
    ERR_load_BIO_strings();         // Load BIO error strings
    OpenSSL_add_all_algorithms();   // Load all available encryption algorithms

    //g_hInst = hInstance;

    hMainDlg = (HWND)CreateDialogParam(hInstance, MAKEINTRESOURCE(IDD_MAIN), 0, MainDialogProc, 0);
    ShowWindow(hMainDlg, nCmdShow);

    /* Message Loop */
    while((ret = GetMessage(&msg, 0, 0, 0)) != 0) {
      if(ret == -1) /* error found */
        return -1;

      if(!IsDialogMessage(hMainDlg, &msg)) {
        TranslateMessage(&msg); /* translate virtual-key messages */
        DispatchMessage(&msg); /* send it to dialog procedure */
      }
    }

    return 0;
}


BOOL CALLBACK MainDialogProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam){
    switch(Message){
        case WM_INITDIALOG:
            onInit(hwnd);
            return TRUE;
        case WM_COMMAND:
            switch(LOWORD(wParam)){
                case IDC_BUTTON_ABOUT:
                    DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc);
                break;
                case IDC_BUTTON_EMAIL_SETTINGS:
                    DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_MAIL_SETTINGS), hwnd, EmailSettingsDlgProc);
                break;
                case IDC_RADIOBUTTON_CH_SITE_RANDOM:
                {
                    HWND hRadio;
                    SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                    hRadio = GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_SITE_SEQ);
                    SendMessage(hRadio, BM_SETCHECK, BST_UNCHECKED, 0);
                    formData.webSiteData.isRandomOrder = true;
                }
                break;
                case IDC_RADIOBUTTON_CH_SITE_SEQ:
                {
                    HWND hRadio;
                    SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                    hRadio = GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_SITE_RANDOM);
                    SendMessage(hRadio, BM_SETCHECK, BST_UNCHECKED, 0);
                    formData.webSiteData.isRandomOrder = false;
                }
                break;
                case IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM:
                {
                    HWND hRadio;
                    SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                    hRadio = GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_KEYWORDS_SEQ);
                    SendMessage(hRadio, BM_SETCHECK, BST_UNCHECKED, 0);
                    formData.googleData.isRandomOrder = true;
                }
                break;
                case IDC_RADIOBUTTON_CH_KEYWORDS_SEQ:
                {
                    HWND hRadio;
                    SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                    hRadio = GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM);
                    SendMessage(hRadio, BM_SETCHECK, BST_UNCHECKED, 0);
                    formData.googleData.isRandomOrder = false;
                }
                break;

                case IDC_RADIOBUTTON_CCS_TCP:
                {
                    HWND hRadio;
                    SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                    hRadio = GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_UDP);
                    SendMessage(hRadio, BM_SETCHECK, BST_UNCHECKED, 0);
                    formData.customServerAndClient.isTCP = true;
                }
                break;
                case IDC_RADIOBUTTON_CCS_UDP:
                {
                    HWND hRadio;
                    SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                    hRadio = GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_TCP);
                    SendMessage(hRadio, BM_SETCHECK, BST_UNCHECKED, 0);
                    formData.customServerAndClient.isTCP = false;
                }
                break;

                case IDC_BUTTON_START:
                {
                    onStart(hwnd);
                }
                break;
                case IDC_CHECKBOX_WRITE_TO_FILE:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_FILENAME, EM_SETREADONLY, TRUE, 0);
                        formData.isLoggingOn = true;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_FILENAME, EM_SETREADONLY, FALSE, 0);
                        formData.isLoggingOn = false;
                    }
                }
                break;
                case IDC_CHECKBOX_TSV_WRITE_TO_FILE:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_TSV_FILENAME, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_BUTTON_TSV_BROWSE, EM_SETREADONLY, TRUE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_TSV_BROWSE), FALSE);
                        formData.isTSV_on = true;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_TSV_FILENAME, EM_SETREADONLY, FALSE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_BUTTON_TSV_BROWSE), TRUE);
                        formData.isTSV_on = false;
                    }
                }
                break;
                case IDC_CHECKBOX_YOUTUBE:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        formData.watchYoutube = false;
                        SendDlgItemMessage(hwnd, IDC_EDIT_YOUTUBE_REPEAT_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_YOUTUBE_REPEAT_MAX, EM_SETREADONLY, TRUE, 0);
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        formData.watchYoutube = true;
                        SendDlgItemMessage(hwnd, IDC_EDIT_YOUTUBE_REPEAT_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_YOUTUBE_REPEAT_MAX, EM_SETREADONLY, FALSE, 0);
                    }
                }
                break;
                case IDC_CHECKBOX_GOOGLE:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        formData.useGoogle = false;
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_DEPTH_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_DEPTH_MAX, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_LOOK_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_LOOK_MAX, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_KEYWORDS_LIST, EM_SETREADONLY, TRUE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM), false);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_KEYWORDS_SEQ), false);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_REPEAT_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_REPEAT_MAX, EM_SETREADONLY, TRUE, 0);
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        formData.useGoogle = true;
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_DEPTH_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_DEPTH_MAX, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_LOOK_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_LOOK_MAX, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_KEYWORDS_LIST, EM_SETREADONLY, FALSE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM), true);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_KEYWORDS_SEQ), true);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_REPEAT_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_GOOGLE_REPEAT_MAX, EM_SETREADONLY, FALSE, 0);
                    }
                }
                break;
                case IDC_CHECKBOX_WEB:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        formData.useLinks = false;
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_DEPTH_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_DEPTH_MAX, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_LOOK_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_LOOK_MAX, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_LIST, EM_SETREADONLY, TRUE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_SITE_RANDOM), false);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_SITE_SEQ), false);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_REPEAT_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_REPEAT_MAX, EM_SETREADONLY, TRUE, 0);
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        formData.useLinks = true;
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_DEPTH_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_DEPTH_MAX, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_LOOK_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_LOOK_MAX, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_LIST, EM_SETREADONLY, FALSE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_SITE_RANDOM), true);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CH_SITE_SEQ), true);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_REPEAT_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_SITE_REPEAT_MAX, EM_SETREADONLY, FALSE, 0);
                    }
                }
                break;
                case IDC_CHECKBOX_RANDOM:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        formData.isRandom = false;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        formData.isRandom = true;
                    }
                }
                break;
                case IDC_CHECKBOX_EMAIL_SMTP_EN:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_TO, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_SUBJECT, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_CONTENT, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_PREP_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_PREP_MAX, EM_SETREADONLY, TRUE, 0);

                        formData.email.isSendOn = false;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_TO, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_SUBJECT, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_CONTENT, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_PREP_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_PREP_MAX, EM_SETREADONLY, FALSE, 0);

                        formData.email.isSendOn = true;
                    }
                }
                break;
                case IDC_CHECKBOX_EMAIL_READ_EN:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_LIST_TIME_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_LIST_TIME_MAX, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_VIEW_TIME_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_VIEW_TIME_MAX, EM_SETREADONLY, TRUE, 0);

                        formData.email.isSendOn = false;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_LIST_TIME_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_LIST_TIME_MAX, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_VIEW_TIME_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_EMAIL_VIEW_TIME_MAX, EM_SETREADONLY, FALSE, 0);

                        formData.email.isSendOn = true;
                    }
                }
                break;
                case IDC_CHECKBOX_CSERVER_ON:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER_PORT, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER_ANSWER_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER_ANSWER_MAX, EM_SETREADONLY, TRUE, 0);
                        if(formData.customServerAndClient.cclient.isOn == false){
                            EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_TCP), false);
                            EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_UDP), false);
                        }

                        formData.customServerAndClient.cserver.isOn = false;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER_PORT, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER_ANSWER_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER_ANSWER_MAX, EM_SETREADONLY, FALSE, 0);

                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_TCP), true);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_UDP), true);

                        formData.customServerAndClient.cserver.isOn = true;
                    }
                }
                break;
                case IDC_CHECKBOX_CCLIENT_ON:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CCLIENT_QUERY_LEN_MAX, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CCLIENT_QUERY_LEN_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CQUERY_FREQ_MIN, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CQUERY_FREQ_MAX, EM_SETREADONLY, TRUE, 0);

                        if(formData.customServerAndClient.cserver.isOn == false){
                            EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_TCP), false);
                            EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_UDP), false);
                        }

                        formData.customServerAndClient.cclient.isOn = false;
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);

                        SendDlgItemMessage(hwnd, IDC_EDIT_CSERVER, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CCLIENT_QUERY_LEN_MAX, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CCLIENT_QUERY_LEN_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CQUERY_FREQ_MIN, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hwnd, IDC_EDIT_CQUERY_FREQ_MAX, EM_SETREADONLY, FALSE, 0);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_TCP), true);
                        EnableWindow(GetDlgItem(hwnd, IDC_RADIOBUTTON_CCS_UDP), true);

                        formData.customServerAndClient.cclient.isOn = true;
                    }
                }
                break;
                case IDC_BUTTON_SAVE_SETTINGS:
                {
                    saveConfiguration(hwnd);
                }
                break;
                case IDC_BUTTON_TSV_BROWSE:
                {
                    OPENFILENAME ofn;
                    char szFileName[MAX_PATH] = "";
                    char workingDirectory[MAX_PATH];

                    ZeroMemory(&ofn, sizeof(ofn));

                    ofn.lStructSize = sizeof(ofn);
                    ofn.hwndOwner = hwnd;
                    ofn.lpstrFilter = "TSV Failai (*.tsv)\0*.tsv\0Visi Failai (*.*)\0*.*\0";
                    ofn.lpstrFile = szFileName;
                    ofn.nMaxFile = MAX_PATH;
                    ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
                    ofn.lpstrDefExt = "tsv";

                    GetCurrentDirectory(MAX_PATH, workingDirectory);

                    if(GetSaveFileName(&ofn))
                    {
                        //Failo vardas issaugotas i szFileName
                        strcpy(formData.TSVfileName, szFileName);
                        SendDlgItemMessage(hwnd, IDC_EDIT_TSV_FILENAME, WM_SETTEXT, 0, (LPARAM)formData.TSVfileName);
                    }

                    SetCurrentDirectory(workingDirectory);
                }
                break;
            }
            break;
        case WM_NOTIFY:
            //switch (((LPNMHDR)lParam)->code){
            //
            //}
            break;
        case WM_CLOSE:
        {
            DestroyWindow(hwnd);
            g_TSV_file.close();
        }
        break;
        case WM_DESTROY:
        {
            PostQuitMessage(0);
        }
        break;
        default:
            return FALSE;
    }
    return TRUE;
}

void onInit(HWND hDlg){

    //Visi checkbox'us pagal default'a uzcheck'inu:
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_WEB), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_GOOGLE), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_YOUTUBE), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_WRITE_TO_FILE), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_TSV_WRITE_TO_FILE), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_RANDOM), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_READ_EN), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_CCLIENT_ON), BM_SETCHECK, BST_CHECKED, 0);
    SendMessage(GetDlgItem(hDlg, IDC_CHECKBOX_CSERVER_ON), BM_SETCHECK, BST_CHECKED, 0);

    //Init combobox
    ComboBox_AddString(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), "Google Chrome 35 : Mozilla/5.0 (Windows NT 6.1), AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36");
    ComboBox_AddString(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), "IE8 : CLIENT STR");
    ComboBox_AddString(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), "Firefox 12 : CLIENTF STR");
    ComboBox_SetCurSel(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), 0);

    SendMessage(GetDlgItem(hDlg, IDC_EDIT_STATUS), EM_SETLIMITTEXT, 0, 0);

    log.setWindowHandle(GetDlgItem(hDlg, IDC_EDIT_STATUS));

    EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_CCLIENT_ON), false); //Nerealizuotam tad isjungta
    EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_CSERVER_ON), false); //Nerealizuota, tad isjungta

    loadConfiguration(hDlg);

}

void saveConfiguration(HWND hDlg){
    //TODO: cia galima sutaupyt kelis simtus baitu gal, vietoj daug siu masyvu nadojant viena
    char repeatMinStr[10]; //Su [4] neveikia (overflow?)
    char repeatMaxStr[10];
    char depthMinStr[10];
    char depthMaxStr[10];
    char lookMinStr[10];
    char lookMaxStr[10];
    char buffer[10]; //Visiem bendras pagaliau :-)

    refreshConfigurationStruct(hDlg);

    WritePrivateProfileString("Common", "useLinks", formData.useLinks?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "useGoogle", formData.useGoogle?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "watchYouTube", formData.watchYoutube?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "isLoggingOn", formData.isLoggingOn?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "logFileName", formData.fileName, CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "tsvFileName", formData.TSVfileName, CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "clientString", formData.clientString, CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "isRandom", formData.isRandom?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "isTSV_on", formData.isTSV_on?"true":"false", CONFIG_FILE_NAME);
    itoa(formData.youtubeRepeatMin, repeatMinStr, 10);
    itoa(formData.youtubeRepeatMax, repeatMaxStr, 10);
    WritePrivateProfileString("Common", "youtubeRepeatMin", repeatMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("Common", "youtubeRepeatMax", repeatMaxStr, CONFIG_FILE_NAME);

    WritePrivateProfileString("WebSite", "isRandomOrder", formData.webSiteData.isRandomOrder?"true":"false", CONFIG_FILE_NAME);
    itoa(formData.webSiteData.depthMin, depthMinStr, 10);
    itoa(formData.webSiteData.depthMax, depthMaxStr, 10);
    itoa(formData.webSiteData.lookMin, lookMinStr, 10);
    itoa(formData.webSiteData.lookMax, lookMaxStr, 10);
    itoa(formData.webSiteData.repeatMin, repeatMinStr, 10);
    itoa(formData.webSiteData.repeatMax, repeatMaxStr, 10);
    WritePrivateProfileString("WebSite", "depthMin", depthMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("WebSite", "depthMax", depthMaxStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("WebSite", "lookMin", lookMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("WebSite", "lookMax", lookMaxStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("WebSite", "repeatMin", repeatMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("WebSite", "repeatMax", repeatMaxStr, CONFIG_FILE_NAME);

    WritePrivateProfileString("Google", "isRandomOrder", formData.googleData.isRandomOrder?"true":"false", CONFIG_FILE_NAME);
    itoa(formData.googleData.depthMin, depthMinStr, 10);
    itoa(formData.googleData.depthMax, depthMaxStr, 10);
    itoa(formData.googleData.lookMin, lookMinStr, 10);
    itoa(formData.googleData.lookMax, lookMaxStr, 10);
    itoa(formData.googleData.repeatMin, repeatMinStr, 10);
    itoa(formData.googleData.repeatMax, repeatMaxStr, 10);
    WritePrivateProfileString("Google", "depthMin", depthMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("Google", "depthMax", depthMaxStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("Google", "lookMin", lookMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("Google", "lookMax", lookMaxStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("Google", "repeatMin", repeatMinStr, CONFIG_FILE_NAME);
    WritePrivateProfileString("Google", "repeatMax", repeatMaxStr, CONFIG_FILE_NAME);

    /* Email */
    WritePrivateProfileString("Email", "isSendOn", formData.email.isSendOn?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "isRecvOn", formData.email.isRecvOn?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "SMTP_useSSL", formData.email.smtpSettings.useSSL?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "SMTP_useAuth", formData.email.smtpSettings.useAuth?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "IMAP_useSSL", formData.email.imapSettings.useSSL?"true":"false", CONFIG_FILE_NAME);

    WritePrivateProfileString("Email", "defaultRecipient", formData.email.To, CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "defaultSubject", formData.email.Subject, CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "defaultMessage", formData.email.Message, CONFIG_FILE_NAME);
    itoa(formData.email.prepTimeMin, buffer, 10);
    WritePrivateProfileString("Email", "prepTimeMin", buffer, CONFIG_FILE_NAME);
    itoa(formData.email.prepTimeMax, buffer, 10);
    WritePrivateProfileString("Email", "prepTimeMax", buffer, CONFIG_FILE_NAME);
    itoa(formData.email.viewTimeMin, buffer, 10);
    WritePrivateProfileString("Email", "viewTimeMin", buffer, CONFIG_FILE_NAME);
    itoa(formData.email.viewTimeMax, buffer, 10);
    WritePrivateProfileString("Email", "viewTimeMax", buffer, CONFIG_FILE_NAME);
    itoa(formData.email.listTimeMin, buffer, 10);
    WritePrivateProfileString("Email", "listTimeMin", buffer, CONFIG_FILE_NAME);
    itoa(formData.email.listTimeMax, buffer, 10);
    WritePrivateProfileString("Email", "listTimeMax", buffer, CONFIG_FILE_NAME);

    WritePrivateProfileString("Email", "IMAP_server", formData.email.imapSettings.server, CONFIG_FILE_NAME);
    itoa(formData.email.imapSettings.port, buffer, 10);
    WritePrivateProfileString("Email", "IMAP_port", buffer, CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "IMAP_user", formData.email.imapSettings.userName, CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "IMAP_password", formData.email.imapSettings.Password, CONFIG_FILE_NAME);

    WritePrivateProfileString("Email", "SMTP_server", formData.email.smtpSettings.server, CONFIG_FILE_NAME);
    itoa(formData.email.smtpSettings.port, buffer, 10);
    WritePrivateProfileString("Email", "SMTP_port", buffer, CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "SMTP_user", formData.email.smtpSettings.userName, CONFIG_FILE_NAME);
    WritePrivateProfileString("Email", "SMTP_password", formData.email.smtpSettings.Password, CONFIG_FILE_NAME);

    WritePrivateProfileString("Email", "From", formData.email.From, CONFIG_FILE_NAME);

    /* Custom client & server */
    WritePrivateProfileString("CClient", "isOn", formData.customServerAndClient.cclient.isOn?"true":"false", CONFIG_FILE_NAME);
    WritePrivateProfileString("CClient", "serverName", formData.customServerAndClient.cclient.serverName, CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cclient.queryLengthMin, buffer, 10);
    WritePrivateProfileString("CClient", "queryLengthMin", buffer, CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cclient.queryLengthMax, buffer, 10);
    WritePrivateProfileString("CClient", "queryLengthMax", buffer, CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cclient.queryPeriodMin, buffer, 10);
    WritePrivateProfileString("CClient", "queryPeriodMin", buffer, CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cclient.queryPeriodMax, buffer, 10);
    WritePrivateProfileString("CClient", "queryPeriodMax", buffer, CONFIG_FILE_NAME);
    //itoa(formData.customServerAndClient.cclient.port, buffer, 10); - TODO: gal visgi reiktu lauka padaryti tam
    //WritePrivateProfileString("CClient", "port", buffer, CONFIG_FILE_NAME);

    WritePrivateProfileString("CServer", "isOn", formData.customServerAndClient.cserver.isOn?"true":"false", CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cserver.port, buffer, 10);
    WritePrivateProfileString("CServer", "port", buffer, CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cserver.answerLengthMin, buffer, 10);
    WritePrivateProfileString("CServer", "answerLengthMin", buffer, CONFIG_FILE_NAME);
    itoa(formData.customServerAndClient.cserver.answerLengthMax, buffer, 10);
    WritePrivateProfileString("CServer", "answerLengthMax", buffer, CONFIG_FILE_NAME);

    WritePrivateProfileString("Common", "areServerAndClientUseTCP", formData.customServerAndClient.isTCP?"true":"false", CONFIG_FILE_NAME);

    /* List'u issaugojimas */
    {
        char webSiteConfStr[sizeof("WebSite") + 10] = "WebSite";
        char keywordsConfStr[sizeof("Keywords") + 10] = "Keywords";
        int id;
        int numOfWebSitesInList = SendMessage(GetDlgItem(hDlg, IDC_EDIT_SITE_LIST), EM_GETLINECOUNT, 0, 0);
        int numOfGoogleKeywords = SendMessage(GetDlgItem(hDlg, IDC_EDIT_GOOGLE_KEYWORDS_LIST), EM_GETLINECOUNT, 0, 0);

        for(id = 0; (id < numOfWebSitesInList) && (id < 100); id++){
            itoa(id, &webSiteConfStr[sizeof("WebSite") - 1], 10);
            WritePrivateProfileString("WebSite", webSiteConfStr, formData.webSiteData.webSiteList[id], CONFIG_FILE_NAME);
        }
        for(id = 0; (id < numOfGoogleKeywords) && (id < 100); id++){
            itoa(id, &keywordsConfStr[sizeof("Keywords") - 1], 10);
            WritePrivateProfileString("Keywords", keywordsConfStr, formData.googleData.keywordsList[id], CONFIG_FILE_NAME);
        }
    }
}

void loadConfiguration(HWND hDlg){
    HWND hCtrl;
//  if(PathFileExists(CONFIG_FILE_NAME)){
    char retString[MAX_PATH];
    ///Gal cia ateityje bus galima isoptimizuoti rasyma i nustatymu struktura, kadangi pabaigoje as vistiek
    ///darau refreshConfiguration()

    //---[ Varnos: ]------------------------------------------------------------------------------------
    GetPrivateProfileString("Common", "useLinks", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.useLinks = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Common", "useGoogle", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.useGoogle = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Common", "watchYouTube", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.watchYoutube = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Common", "isLoggingOn", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.isLoggingOn = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Common", "isRandom", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.isRandom = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Common", "isTSV_on", "false", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.isTSV_on = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Email", "isSendOn", "false", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.email.isSendOn = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Email", "isRecvOn", "false", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.email.isRecvOn = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Email", "SMTP_useSSL", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.email.smtpSettings.useSSL = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Email", "SMTP_useAuth", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.email.smtpSettings.useAuth = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("Email", "IMAP_useSSL", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.email.imapSettings.useSSL = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("CServer", "isOn", "false", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.customServerAndClient.cserver.isOn = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    GetPrivateProfileString("CClient", "isOn", "false", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.customServerAndClient.cclient.isOn = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);

    if(!formData.watchYoutube){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_YOUTUBE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_YOUTUBE));
    }
    if(!formData.useLinks){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_WEB, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_WEB));
    }
    if(!formData.useGoogle){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_GOOGLE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_GOOGLE));
    }
    if(!formData.isLoggingOn){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_WRITE_TO_FILE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_WRITE_TO_FILE));
    }
    if(!formData.isRandom){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_RANDOM, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_RANDOM));
    }
    if(!formData.isTSV_on){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_TSV_WRITE_TO_FILE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_TSV_WRITE_TO_FILE));
    }
    if(!formData.email.isSendOn){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_EMAIL_SMTP_EN, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN));
    }
    if(!formData.email.isRecvOn){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_EMAIL_READ_EN, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_READ_EN));
    }
    if(!formData.customServerAndClient.cclient.isOn){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_CCLIENT_ON, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_CCLIENT_ON));
    }
    if(!formData.customServerAndClient.cserver.isOn){
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_CSERVER_ON, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_CSERVER_ON));
    }

    //---[ Radio ]--------------------------------------------------------------------------------------
    GetPrivateProfileString("WebSite", "isRandomOrder", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.webSiteData.isRandomOrder = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    if(formData.webSiteData.isRandomOrder)
        SendMessage(hDlg, WM_COMMAND, IDC_RADIOBUTTON_CH_SITE_RANDOM, (LPARAM)GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_SITE_RANDOM));
    else
        SendMessage(hDlg, WM_COMMAND, IDC_RADIOBUTTON_CH_SITE_SEQ, (LPARAM)GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_SITE_SEQ));

    GetPrivateProfileString("Google", "isRandomOrder", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.googleData.isRandomOrder = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    if(formData.googleData.isRandomOrder)
        SendMessage(hDlg, WM_COMMAND, IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM, (LPARAM)GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM));
    else
        SendMessage(hDlg, WM_COMMAND, IDC_RADIOBUTTON_CH_KEYWORDS_SEQ, (LPARAM)GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_KEYWORDS_SEQ));

    GetPrivateProfileString("Common", "areServerAndClientUseTCP", "true", retString, MAX_PATH, CONFIG_FILE_NAME);
    formData.customServerAndClient.isTCP = (((retString[0] == 't') || (retString[0] == 'T'))? true : false);
    if(formData.customServerAndClient.isTCP)
        SendMessage(hDlg, WM_COMMAND, IDC_RADIOBUTTON_CCS_TCP, (LPARAM)GetDlgItem(hDlg, IDC_RADIOBUTTON_CCS_TCP));
    else
        SendMessage(hDlg, WM_COMMAND, IDC_RADIOBUTTON_CCS_UDP, (LPARAM)GetDlgItem(hDlg, IDC_RADIOBUTTON_CCS_UDP));


    //---[ Edit ]---------------------------------------------------------------------------------------
    GetPrivateProfileString("Common", "logFileName", "ktrsg.log", retString, MAX_PATH, CONFIG_FILE_NAME);
    strcpy(formData.fileName, retString);
    SetDlgItemText(hDlg, IDC_EDIT_FILENAME, formData.fileName);

    GetPrivateProfileString("Common", "tsvFileName", "ktrsg.tsv", retString, MAX_PATH, CONFIG_FILE_NAME);
    strcpy(formData.TSVfileName, retString);
    SetDlgItemText(hDlg, IDC_EDIT_TSV_FILENAME, formData.TSVfileName);

    GetPrivateProfileString("Common", "clientString", DEFAULT_CLIENT_STRING, retString, MAX_CLIENT_STRING_LEN, CONFIG_FILE_NAME);
    formData.clientString = (LPSTR)GlobalAlloc(GPTR, MAX_CLIENT_STRING_LEN + 1);
    if(formData.clientString){
        int index;
        strcpy(formData.clientString, retString);
        index = ComboBox_FindStringExact(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), 0, retString);
        if(index == CB_ERR){
            index = ComboBox_AddString(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), retString);
        }
        ComboBox_SetCurSel(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), index);
    }
    else{
        MessageBoxA(hDlg, "Klaida", "Nepavyko isskirti atminties clientString.", MB_ICONERROR | MB_OK);
    }
    formData.webSiteData.depthMin = GetPrivateProfileInt("WebSite", "depthMin", 3, CONFIG_FILE_NAME);
    formData.webSiteData.depthMax = GetPrivateProfileInt("WebSite", "depthMax", 8, CONFIG_FILE_NAME);
    formData.webSiteData.lookMin = GetPrivateProfileInt("WebSite", "lookMin", 4000, CONFIG_FILE_NAME);
    formData.webSiteData.lookMax = GetPrivateProfileInt("WebSite", "lookMax", 8000, CONFIG_FILE_NAME);
    formData.webSiteData.repeatMin = GetPrivateProfileInt("WebSite", "repeatMin", 1, CONFIG_FILE_NAME);
    formData.webSiteData.repeatMax = GetPrivateProfileInt("WebSite", "repeatMax", 1, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_SITE_DEPTH_MIN, formData.webSiteData.depthMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_SITE_DEPTH_MAX, formData.webSiteData.depthMax, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_SITE_LOOK_MIN, formData.webSiteData.lookMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_SITE_LOOK_MAX, formData.webSiteData.lookMax, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_SITE_REPEAT_MIN, formData.webSiteData.repeatMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_SITE_REPEAT_MAX, formData.webSiteData.repeatMax, FALSE);

    formData.googleData.depthMin = GetPrivateProfileInt("Google", "depthMin", 4, CONFIG_FILE_NAME);
    formData.googleData.depthMax = GetPrivateProfileInt("Google", "depthMax", 8, CONFIG_FILE_NAME);
    formData.googleData.lookMin = GetPrivateProfileInt("Google", "lookMin", 4000, CONFIG_FILE_NAME);
    formData.googleData.lookMax = GetPrivateProfileInt("Google", "lookMax", 8000, CONFIG_FILE_NAME);
    formData.googleData.repeatMin = GetPrivateProfileInt("Google", "repeatMin", 1, CONFIG_FILE_NAME);
    formData.googleData.repeatMax = GetPrivateProfileInt("Google", "repeatMax", 1, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_DEPTH_MIN, formData.googleData.depthMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_DEPTH_MAX, formData.googleData.depthMax, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_LOOK_MIN, formData.googleData.lookMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_LOOK_MAX, formData.googleData.lookMax, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_REPEAT_MIN, formData.googleData.repeatMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_REPEAT_MAX, formData.googleData.repeatMax, FALSE);

    formData.youtubeRepeatMin = GetPrivateProfileInt("Common", "youtubeRepeatMin", 1, CONFIG_FILE_NAME);
    formData.youtubeRepeatMax = GetPrivateProfileInt("Common", "youtubeRepeatMax", 1, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_YOUTUBE_REPEAT_MIN, formData.youtubeRepeatMin, FALSE);
    SetDlgItemInt(hDlg, IDC_EDIT_YOUTUBE_REPEAT_MAX, formData.youtubeRepeatMax, FALSE);

    /* Email */
    GetPrivateProfileString("Email", "defaultRecipient", "vartotojas@gmail.com", retString, MAX_EMAIL_ADDRESS_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.To, retString);
    SetDlgItemText(hDlg, IDC_EDIT_EMAIL_TO, formData.email.To);
    GetPrivateProfileString("Email", "defaultSubject", "Tinklo apkrovos generatoriaus atsiustas laiskas", retString, MAX_EMAIL_SUBJECT_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.Subject, retString);
    SetDlgItemText(hDlg, IDC_EDIT_EMAIL_SUBJECT, formData.email.Subject);
    GetPrivateProfileString("Email", "defaultMessage", "Kompiuteriu tinklo realaus srauto generatoriaus automatiskai atsiustas laiskas. Galima istrinti.", retString, MAX_EMAIL_MESSAGE_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.Message, retString);
    SetDlgItemText(hDlg, IDC_EDIT_EMAIL_CONTENT, formData.email.Message);
    formData.email.prepTimeMin = GetPrivateProfileInt("Email", "prepTimeMin", 5, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_EMAIL_PREP_MIN, formData.email.prepTimeMin, FALSE);
    formData.email.prepTimeMax = GetPrivateProfileInt("Email", "prepTimeMax", 10, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_EMAIL_PREP_MAX, formData.email.prepTimeMax, FALSE);
    formData.email.viewTimeMin = GetPrivateProfileInt("Email", "viewTimeMin", 5, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_EMAIL_VIEW_TIME_MIN, formData.email.viewTimeMin, FALSE);
    formData.email.viewTimeMax = GetPrivateProfileInt("Email", "viewTimeMax", 10, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_EMAIL_VIEW_TIME_MAX, formData.email.viewTimeMax, FALSE);
    formData.email.listTimeMin = GetPrivateProfileInt("Email", "listTimeMin", 5, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_EMAIL_LIST_TIME_MIN, formData.email.listTimeMin, FALSE);
    formData.email.listTimeMax = GetPrivateProfileInt("Email", "listTimeMax", 10, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_EMAIL_LIST_TIME_MAX, formData.email.listTimeMax, FALSE);

    formData.customServerAndClient.cserver.port = GetPrivateProfileInt("CServer", "port", 1555, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CSERVER_PORT, formData.customServerAndClient.cserver.port, FALSE);
    formData.customServerAndClient.cserver.answerLengthMin = GetPrivateProfileInt("CServer", "answerLengthMin", 1024, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CSERVER_ANSWER_MIN, formData.customServerAndClient.cserver.answerLengthMin, FALSE);
    formData.customServerAndClient.cserver.answerLengthMax = GetPrivateProfileInt("CServer", "answerLengthMax", 8192, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CSERVER_ANSWER_MAX, formData.customServerAndClient.cserver.answerLengthMax, FALSE);

    formData.customServerAndClient.cclient.queryLengthMin = GetPrivateProfileInt("CClient", "queryLengthMin", 10, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CCLIENT_QUERY_LEN_MIN, formData.customServerAndClient.cclient.queryLengthMin, FALSE);
    formData.customServerAndClient.cclient.queryLengthMax = GetPrivateProfileInt("CClient", "queryLengthMax", 100, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CCLIENT_QUERY_LEN_MAX, formData.customServerAndClient.cclient.queryLengthMax, FALSE);
    formData.customServerAndClient.cclient.queryPeriodMin = GetPrivateProfileInt("CClient", "queryPeriodMin", 10, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CQUERY_FREQ_MIN, formData.customServerAndClient.cclient.queryPeriodMin, FALSE);
    formData.customServerAndClient.cclient.queryPeriodMax = GetPrivateProfileInt("CClient", "queryPeriodMax", 10, CONFIG_FILE_NAME);
    SetDlgItemInt(hDlg, IDC_EDIT_CQUERY_FREQ_MAX, formData.customServerAndClient.cclient.queryPeriodMax, FALSE);
    GetPrivateProfileString("CClient", "serverName", "localhost:1555", retString, MAX_DOMAIN_NAME_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.customServerAndClient.cclient.serverName, retString);
    SetDlgItemText(hDlg, IDC_EDIT_CSERVER, formData.customServerAndClient.cclient.serverName);

    /* Email settings (atskiras dialogas) */
    GetPrivateProfileString("Email", "SMTP_server", "smtp.gmail.com", retString, MAX_DOMAIN_NAME_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.smtpSettings.server, retString);
    formData.email.smtpSettings.port = GetPrivateProfileInt("Email", "SMTP_port", 465, CONFIG_FILE_NAME);
    GetPrivateProfileString("Email", "SMTP_user", "vartotojas", retString, MAX_USERNAME_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.smtpSettings.userName, retString);
    GetPrivateProfileString("Email", "SMTP_password", "", retString, MAX_PASSWORD_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.smtpSettings.Password, retString);

    GetPrivateProfileString("Email", "IMAP_server", "imap.gmail.com", retString, MAX_DOMAIN_NAME_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.imapSettings.server, retString);
    formData.email.imapSettings.port = GetPrivateProfileInt("Email", "IMAP_port", 993, CONFIG_FILE_NAME);
    GetPrivateProfileString("Email", "IMAP_user", "vartotojas", retString, MAX_USERNAME_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.imapSettings.userName, retString);
    GetPrivateProfileString("Email", "IMAP_password", "", retString, MAX_PASSWORD_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.imapSettings.Password, retString);

    GetPrivateProfileString("Email", "From", "vartotojas@gmail.com", retString, MAX_EMAIL_ADDRESS_LENGTH, CONFIG_FILE_NAME);
    strcpy(formData.email.From, retString);

    //---[ Lists ]--------------------------------------------------------------------------------------
    {
        string webSiteNames, keywords;
        char webSiteConfStr[sizeof("WebSite") + 10] = "WebSite";
        char keywordsConfStr[sizeof("Keywords") + 10] = "Keywords";
        char resultStr[MAX_LINK_LENGTH];
        int id;

        for(id = 0; id < 100; id++){ //!!!
            itoa(id, &webSiteConfStr[sizeof("WebSite") - 1], 10);
            if(!GetPrivateProfileString("WebSite", webSiteConfStr, NULL, resultStr, MAX_LINK_LENGTH, CONFIG_FILE_NAME)){
                break;
            }
            webSiteNames += resultStr;
            webSiteNames += "\r\n";
        }
        formData.webSiteData.totalWebSites = id;
        SetDlgItemText(hDlg, IDC_EDIT_SITE_LIST, webSiteNames.c_str());

        for(id = 0; id < 100; id++){ //!!!
            itoa(id, &keywordsConfStr[sizeof("Keywords") - 1], 10);
            if(!GetPrivateProfileString("Keywords", keywordsConfStr, NULL, resultStr, MAX_KEYWORD_LENGTH, CONFIG_FILE_NAME)){
                break;
            }
            keywords += resultStr;
            keywords += "\r\n";
        }
        formData.googleData.totalKeywords = id;
        SetDlgItemText(hDlg, IDC_EDIT_GOOGLE_KEYWORDS_LIST, keywords.c_str());
    }

    refreshConfigurationStruct(hDlg);
}

void refreshConfigurationStruct(HWND hDlg){
    BOOL isCorrect;
    int length;
    LPSTR SiteList; //char *
    LPSTR KeywordsList;
    int lineCount;
    HWND hCtl;

//    hCtl = GetDlgItem(hwnd, IDC_EDIT_SITE_DEPTH_MIN);
    formData.webSiteData.depthMin = GetDlgItemInt(hDlg, IDC_EDIT_SITE_DEPTH_MIN, NULL, FALSE);
    formData.webSiteData.depthMax = GetDlgItemInt(hDlg, IDC_EDIT_SITE_DEPTH_MAX, NULL, FALSE);
    formData.webSiteData.lookMin  = GetDlgItemInt(hDlg, IDC_EDIT_SITE_LOOK_MIN, NULL, FALSE);
    formData.webSiteData.lookMax  = GetDlgItemInt(hDlg, IDC_EDIT_SITE_LOOK_MAX, NULL, FALSE);
    formData.webSiteData.repeatMin  = GetDlgItemInt(hDlg, IDC_EDIT_SITE_REPEAT_MIN, NULL, FALSE);
    formData.webSiteData.repeatMax  = GetDlgItemInt(hDlg, IDC_EDIT_SITE_REPEAT_MAX, NULL, FALSE);

    formData.googleData.depthMin = GetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_DEPTH_MIN, NULL, FALSE);
    formData.googleData.depthMax = GetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_DEPTH_MAX, NULL, FALSE);
    formData.googleData.lookMin  = GetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_LOOK_MIN, NULL, FALSE);
    formData.googleData.lookMax  = GetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_LOOK_MAX, NULL, FALSE);
    formData.googleData.repeatMin  = GetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_REPEAT_MIN, NULL, FALSE);
    formData.googleData.repeatMax  = GetDlgItemInt(hDlg, IDC_EDIT_GOOGLE_REPEAT_MAX, NULL, FALSE);

    formData.youtubeRepeatMin  = GetDlgItemInt(hDlg, IDC_EDIT_YOUTUBE_REPEAT_MIN, NULL, FALSE);
    formData.youtubeRepeatMax  = GetDlgItemInt(hDlg, IDC_EDIT_YOUTUBE_REPEAT_MAX, NULL, FALSE);

    /* Email client */
    formData.email.isSendOn = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN);
    formData.email.isRecvOn = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_EMAIL_READ_EN);
    //formData.email.smtpSettings.useSSL = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_SET_SMTP_SSL);
    GetDlgItemText(hDlg, IDC_EDIT_EMAIL_TO, formData.email.To, MAX_EMAIL_ADDRESS_LENGTH);
    GetDlgItemText(hDlg, IDC_EDIT_EMAIL_SUBJECT, formData.email.Subject, MAX_EMAIL_SUBJECT_LENGTH);
    GetDlgItemText(hDlg, IDC_EDIT_EMAIL_CONTENT, formData.email.Message, MAX_EMAIL_MESSAGE_LENGTH);
    formData.email.prepTimeMin  = GetDlgItemInt(hDlg, IDC_EDIT_EMAIL_PREP_MIN, NULL, FALSE);
    formData.email.prepTimeMax  = GetDlgItemInt(hDlg, IDC_EDIT_EMAIL_PREP_MAX, NULL, FALSE);
    formData.email.listTimeMin  = GetDlgItemInt(hDlg, IDC_EDIT_EMAIL_LIST_TIME_MIN, NULL, FALSE);
    formData.email.listTimeMax  = GetDlgItemInt(hDlg, IDC_EDIT_EMAIL_LIST_TIME_MAX, NULL, FALSE);
    formData.email.viewTimeMin  = GetDlgItemInt(hDlg, IDC_EDIT_EMAIL_VIEW_TIME_MIN, NULL, FALSE);
    formData.email.viewTimeMax  = GetDlgItemInt(hDlg, IDC_EDIT_EMAIL_VIEW_TIME_MAX, NULL, FALSE);

    /* Custom server */
    formData.customServerAndClient.cserver.isOn = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_CSERVER_ON);
    formData.customServerAndClient.cserver.port = GetDlgItemInt(hDlg, IDC_EDIT_CSERVER_PORT, NULL, FALSE);
    formData.customServerAndClient.cserver.answerLengthMin = GetDlgItemInt(hDlg, IDC_EDIT_CSERVER_ANSWER_MIN, NULL, FALSE);
    formData.customServerAndClient.cserver.answerLengthMax = GetDlgItemInt(hDlg, IDC_EDIT_CSERVER_ANSWER_MAX, NULL, FALSE);

    /* Custom client */
    formData.customServerAndClient.cclient.isOn = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_CCLIENT_ON);
    GetDlgItemText(hDlg, IDC_EDIT_CSERVER, formData.customServerAndClient.cclient.serverName, MAX_USERNAME_LENGTH);
    formData.customServerAndClient.cclient.queryLengthMin = GetDlgItemInt(hDlg, IDC_EDIT_CCLIENT_QUERY_LEN_MIN, NULL, FALSE);
    formData.customServerAndClient.cclient.queryLengthMax = GetDlgItemInt(hDlg, IDC_EDIT_CCLIENT_QUERY_LEN_MAX, NULL, FALSE);
    formData.customServerAndClient.cclient.queryPeriodMin = GetDlgItemInt(hDlg, IDC_EDIT_CQUERY_FREQ_MIN, NULL, FALSE);
    formData.customServerAndClient.cclient.queryPeriodMax = GetDlgItemInt(hDlg, IDC_EDIT_CQUERY_FREQ_MAX, NULL, FALSE);

    //-----------------
    {
        int selection, numCharacters;
        selection = ComboBox_GetCurSel(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT));
        numCharacters = ComboBox_GetLBTextLen(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), selection);
        if(numCharacters == CB_ERR){ //Niekas nepasirinkta - teksta ivedziau pats
            ///TODO: Kazkodel negalima gauti teksto is CB, kuri ivedziau pats. Kolkas resurse nurodziau, kad
            ///nebutu galimybes paciam ivest. Veliau taisyti. Tad siuo metu sis atvejis niekad neturi ivykti.
            ///Tam resurse CBS_DROPDOWN pakeistas i CBS_DROPDOWNLIST.
            /// formData.clientString = (LPSTR)GlobalAlloc(GPTR, MAX_CLIENT_STRING_LEN + 1); 20140831 - atmintis rezervuota loadConfiguration()
            if(formData.clientString){
                GetDlgItemText(hDlg, IDC_COMBOBOX_CLIENT, formData.clientString, MAX_CLIENT_STRING_LEN);
            }
        }
        else{
            /// formData.clientString = (LPSTR)GlobalAlloc(GPTR, MAX_CLIENT_STRING_LEN + 1); 20140831 - atmintis rezervuota loadConfiguration()
            if(formData.clientString){
                ComboBox_GetLBText(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), selection, formData.clientString);
            }
        }
    }

    GetDlgItemText(hDlg, IDC_EDIT_FILENAME, formData.fileName, MAX_PATH);
    GetDlgItemText(hDlg, IDC_EDIT_TSV_FILENAME, formData.TSVfileName, MAX_PATH);

    formData.isLoggingOn = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_WRITE_TO_FILE);
    formData.isTSV_on = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_TSV_WRITE_TO_FILE);
    formData.isRandom = IsDlgButtonChecked(hDlg, IDC_CHECKBOX_RANDOM);

    /* Tinklapiu sarasas */
    length = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT_SITE_LIST));
    SiteList = (LPSTR)GlobalAlloc(GPTR, length + 1);
    if(SiteList == NULL){
        MessageBoxA(hDlg, "Atminties rezervavimo klaida", "Klaida", MB_OK|MB_ICONERROR);
        //amen TODO. TODO: pranesima kraut is resurso
    }
    lineCount = SendDlgItemMessage(hDlg, IDC_EDIT_SITE_LIST, EM_GETLINECOUNT, 0, 0);
    #ifdef DEBUG
    log << "lineCount=" << lineCount << "GetDlgItemInt." << endLogRec;
    #endif
    formData.webSiteData.webSiteList = (char**)GlobalAlloc(GPTR, lineCount * sizeof(char*));
    for(int x = 0; x < lineCount; x++){
        formData.webSiteData.webSiteList[x] = (char *)GlobalAlloc(GPTR, MAX_LINK_LENGTH);
    }

    GetDlgItemText(hDlg, IDC_EDIT_SITE_LIST, SiteList, length + 1);
    for(int pos = 0; pos < length; pos++){
        if((SiteList[pos] == '\n') || (SiteList[pos] == '\r')) {
            SiteList[pos] = '\0';
        }
    }
    for(int pos = 0, posList = 0; pos < length; pos++){
        if(SiteList[pos]){
            strcpy(formData.webSiteData.webSiteList[posList++], &SiteList[pos]);
            pos += strlen(&SiteList[pos]);
        }
    }
    formData.webSiteData.totalWebSites = lineCount; //20140901

    #ifdef DEBUG
    for(int x = 0; x < lineCount; x++){
        log << x << ": " << formData.webSiteData.webSiteList[x] << endLogRec;
    }
    #endif

    GlobalFree(SiteList);

    /* Keywords sarasas */
    length = GetWindowTextLength(GetDlgItem(hDlg, IDC_EDIT_GOOGLE_KEYWORDS_LIST));
    KeywordsList = (LPSTR)GlobalAlloc(GPTR, length + 1);
    if(KeywordsList == NULL){
        MessageBoxA(hDlg, "Atminties rezervavimo klaida (KeywordsList).", "Klaida", MB_OK|MB_ICONERROR);
        //amen TODO
    }
    lineCount = SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_KEYWORDS_LIST, EM_GETLINECOUNT, 0, 0);
    #ifdef DEBUG
    log << "lineCount=" << lineCount << ".";
    #endif // DEBUG
    formData.googleData.keywordsList = (char**)GlobalAlloc(GPTR, lineCount*sizeof(char*));
    for(int x = 0; x < lineCount; x++){
        formData.googleData.keywordsList[x] = (char *)GlobalAlloc(GPTR, MAX_KEYWORD_LENGTH);
    }

    GetDlgItemText(hDlg, IDC_EDIT_GOOGLE_KEYWORDS_LIST, KeywordsList, length + 1);
    for(int pos = 0; pos < length; pos++){
        if((KeywordsList[pos] == '\n') || (KeywordsList[pos] == '\r')) {
            KeywordsList[pos] = '\0';
        }
    }
    for(int pos = 0, posList = 0; pos < length; pos++){
        if(KeywordsList[pos]){
            strcpy(formData.googleData.keywordsList[posList++], &KeywordsList[pos]);
            pos += strlen(&KeywordsList[pos]);
        }
    }
    formData.googleData.totalKeywords = lineCount; //20140901

    GlobalFree(KeywordsList);
}

void onStart(HWND hDlg){
    if((!gStarted) && gStopped){ //&& UZBAGTA <-- TODO:
        //Nepradetas
        refreshConfigurationStruct(hDlg);
        if(log.isFileLoggingOn){
            log.stopFileLogging();
        }
        if(formData.isLoggingOn){
            log.setFileName(formData.fileName);
        }
        if(formData.isTSV_on){
                cout << "----" << formData.TSVfileName << endl;
           //     system("pause");
            g_TSV_file.close(); //Del visa ko, jei buvo atidarytas anksciau
            g_TSV_file.open(formData.TSVfileName);
            if(g_TSV_file.good()){
                int result = MessageBoxA(hDlg, "TSV Failas jau egzistuoja. Pildyti? Jei ne, tai perrasyti.", "Perspejimas", MB_YESNOCANCEL|MB_ICONWARNING);
                g_TSV_file.close();
                if(result == IDYES){
                    g_TSV_file.open(formData.TSVfileName, fstream::out | std::fstream::app);
                    if(!g_TSV_file.good()){
                        MessageBoxA(hDlg, "Nepavyko atidaryti failo rasymui.", "Klaida", MB_OK|MB_ICONERROR);
                        return;
                    }
                    //SendDlgItemMessage(hDlg, IDC_EDIT_TSV_FILENAME, WM_SETTEXT, 0, (LPARAM)formData.CSVfileName);
                }
                else if(result == IDNO){
                    g_TSV_file.open(formData.TSVfileName, fstream::out | fstream::trunc);
                    if(!g_TSV_file.good()){
                        MessageBoxA(hDlg, "Nepavyko atidaryti failo perrasymui (overwrite).", "Klaida", MB_OK|MB_ICONERROR);
                        return;
                    }
                    //SendDlgItemMessage(hDlg, IDC_EDIT_TSV_FILENAME, WM_SETTEXT, 0, (LPARAM)formData.TSVfileName);
                }
                else{
                    return;
                }
            }
            else{
                //Failas neegzistuoja, tad sukuriamas.
                g_TSV_file.open(formData.TSVfileName, fstream::out);
                if(!g_TSV_file.good()){
                    MessageBoxA(hDlg, "Nepavyko sukurti failo rasymui.", "Klaida", MB_OK|MB_ICONERROR);
                    return;
                }
                g_TSV_file << "Data\tLaikas\tUNIX epocha\tMano IP\tServerio IP\tMano Port\tServerio Port\tIssiusta baitu\tPriimta baitu\tURL" << endl;
            }
        }

        if(inputCheck(hDlg)){

            log << "===================================================================" << endLogRec;
            log << ";  KTRSG v" << VERSION << " (" << DATE << ")                                        ;" << endLogRec;
            log << ";  Kompiuteriu Tinklo Realaus Srauto Generatoriaus LOG'as         ;" << endLogRec;
            log << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endLogRec;
            gStarted = true;
            gStopped = false;
            SendDlgItemMessage(hDlg, IDC_BUTTON_START, WM_SETTEXT, 0, (LPARAM)"Baigti"); //RES!
            //loadConfiguration(hDlg);
            writeLogDataLine(NULL, NULL, true); //Header'is loge
            HThread_Main = CreateThread(NULL, 0, Thread_Main, hDlg, 0, NULL);
            if ( HThread_Main == NULL) {
                gStarted = false;
                ///ExitProcess(0);
            }

            //Padaroma, kad visi control butu neaktyvus
            SendDlgItemMessage(hDlg, IDC_EDIT_FILENAME, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_YOUTUBE_REPEAT_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_YOUTUBE_REPEAT_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_DEPTH_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_DEPTH_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_LOOK_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_LOOK_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_KEYWORDS_LIST, EM_SETREADONLY, TRUE, 0);
            EnableWindow(GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM), false);
            EnableWindow(GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_KEYWORDS_SEQ), false);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_REPEAT_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_GOOGLE_REPEAT_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_DEPTH_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_DEPTH_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_LOOK_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_LOOK_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_LIST, EM_SETREADONLY, TRUE, 0);
            EnableWindow(GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_SITE_RANDOM), false);
            EnableWindow(GetDlgItem(hDlg, IDC_RADIOBUTTON_CH_SITE_SEQ), false);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_REPEAT_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_SITE_REPEAT_MAX, EM_SETREADONLY, TRUE, 0);
            EnableWindow(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_GOOGLE), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_RANDOM), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_WEB), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_WRITE_TO_FILE), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_TSV_WRITE_TO_FILE), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_YOUTUBE), FALSE);
            SendDlgItemMessage(hDlg, IDC_EDIT_TSV_FILENAME, EM_SETREADONLY, TRUE, 0);

            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_TO, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_SUBJECT, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_CONTENT, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_PREP_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_PREP_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_VIEW_TIME_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_VIEW_TIME_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_LIST_TIME_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_EMAIL_LIST_TIME_MAX, EM_SETREADONLY, TRUE, 0);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_READ_EN), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_EMAIL_SETTINGS), FALSE);

            SendDlgItemMessage(hDlg, IDC_EDIT_CSERVER_PORT, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CSERVER_ANSWER_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CSERVER_ANSWER_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CSERVER, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CQUERY_FREQ_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CQUERY_FREQ_MAX, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CCLIENT_QUERY_LEN_MIN, EM_SETREADONLY, TRUE, 0);
            SendDlgItemMessage(hDlg, IDC_EDIT_CCLIENT_QUERY_LEN_MAX, EM_SETREADONLY, TRUE, 0);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_CCLIENT_ON), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_CSERVER_ON), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_TSV_BROWSE), FALSE);
            EnableWindow(GetDlgItem(hDlg, IDC_RADIOBUTTON_CCS_TCP), false);
            EnableWindow(GetDlgItem(hDlg, IDC_RADIOBUTTON_CCS_UDP), false);

        }
    }
    else if(gStarted && (!gStopped)){
        //Vykdomas, bet dar nebuvo nepradetas stabdyti
        gStarted = false;
        SendDlgItemMessage(hDlg, IDC_BUTTON_START, WM_SETTEXT, 0, (LPARAM)"Stabdoma..."); //RESURSE
        EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_START), FALSE);
    }
    else{
        gStopped = true;
        EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_START), TRUE);
        SendDlgItemMessage(hDlg, IDC_BUTTON_START, WM_SETTEXT, 0, (LPARAM)"Pradeti"); //DET I RESURSA STRINGA

        //Padaryti, kad butu aktyvus tie, kurie turi buti aktyvus
        SendDlgItemMessage(hDlg, IDC_EDIT_FILENAME, EM_SETREADONLY, FALSE, 0);
        EnableWindow(GetDlgItem(hDlg, IDC_COMBOBOX_CLIENT), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_GOOGLE), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_RANDOM), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_WEB), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_WRITE_TO_FILE), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_YOUTUBE), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_READ_EN), TRUE);
        EnableWindow(GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN), TRUE);
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_GOOGLE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_GOOGLE));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_GOOGLE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_GOOGLE));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_WEB, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_WEB));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_WEB, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_WEB));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_YOUTUBE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_YOUTUBE));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_YOUTUBE, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_YOUTUBE));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_EMAIL_READ_EN, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_READ_EN));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_EMAIL_READ_EN, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_READ_EN));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_EMAIL_SMTP_EN, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN));
        SendMessage(hDlg, WM_COMMAND, IDC_CHECKBOX_EMAIL_SMTP_EN, (LPARAM)GetDlgItem(hDlg, IDC_CHECKBOX_EMAIL_SMTP_EN));

        g_TSV_file.close();
    }
}

void browseWebSite(bool &started){
    unsigned int curSite, desiredIterrations, iterrationNum = 0;
    WebSite ws;

    #ifdef DEBUG
    log <<"browseWebSite(). started=" << started << endLogRec;
    #endif

    (formData.webSiteData.repeatMin == formData.webSiteData.repeatMax)?
    (desiredIterrations = formData.webSiteData.repeatMin):
    (desiredIterrations = (rand() % (formData.webSiteData.repeatMax - formData.webSiteData.repeatMin)) + formData.webSiteData.repeatMin);
    #ifdef DEBUG
    log << "Desired website itterations: " << desiredIterrations << endLogRec;
    #endif
    if(formData.clientString != NULL){ //if() del visa ko, kad nebutu netiketumu del klaidu programoje
        char *ua = new char[strlen(formData.clientString) + 1];
        strcpy(ua, formData.clientString);
        char *uaStart = NULL;
        uaStart = strchr(ua, ':');
        if(uaStart == NULL){
            uaStart = ua;
        }
        else{
            uaStart+=2;
        }
        #ifdef DEBUG
        log << "USER AGENT: " << uaStart << endLogRec;
        #endif
        ws.SetUserAgent(uaStart);
    }
    else{
        MessageBoxA(NULL, "formData.clientString != NULL", "Klaida", MB_OK | MB_ICONERROR);
    }
    curSite = 0;
    while((iterrationNum < desiredIterrations) && started){
        log << "iejo i while" << endLogRec;       if(formData.webSiteData.isRandomOrder && formData.webSiteData.totalWebSites){
            curSite = rand() % formData.webSiteData.totalWebSites;
        }
        else if (formData.webSiteData.totalWebSites == 0){
            #ifdef DEBUG
            log << "TINKLAPIU SARASE NERA!!!" << endLogRec;
            #endif
            break; //Tinklapiu sarase nera.
        }
        #ifdef DEBUG
        log << "PASIRINKTO SAITO NR: " << curSite << endLogRec;
        #endif

        if(!ws.SetName(formData.webSiteData.webSiteList[curSite])){
            log << "Nepavyko prisijungti. Galbut tinklapis neegzistuoja arba nera rysio." << endLogRec;
            break;
        }
        ws.Browse(formData.webSiteData.depthMin, formData.webSiteData.depthMax, formData.webSiteData.lookMin, formData.webSiteData.lookMax);
        #ifdef DEBUG
        log << "NARSYMAS BAIGTAS" <<endLogRec;
        #endif // DEBUG
        if(!formData.webSiteData.isRandomOrder){
            curSite++;
            if(curSite == formData.webSiteData.totalWebSites){
                curSite = 0;
                iterrationNum++;
            }
        }
        else if(formData.webSiteData.isRandomOrder){
            iterrationNum++;
        }
    }
}

void searchGoogle(bool &started){
    unsigned int curKeyword, desiredIterrations, iterrationNum = 0;
    Google google; //Cia atidaro pagrindini psl
    #ifdef DEBUG
    log <<" searchGoogle()" << endLogRec;
    #endif

    /* GOOGLE TEST */
    (formData.googleData.repeatMin == formData.googleData.repeatMax)?
    (desiredIterrations = formData.googleData.repeatMin):
    (desiredIterrations = (rand() % (formData.googleData.repeatMax - formData.googleData.repeatMin)) + formData.googleData.repeatMin);
    #ifdef DEBUG
    log << "Desired google itterations: " << desiredIterrations << endLogRec;
    #endif
    if(formData.clientString != NULL){ //if() del visa ko, kad nebutu netiketumu del klaidu programoje
        char *ua = new char[strlen(formData.clientString)];
        char *uaStart = NULL;
        strcpy(ua, formData.clientString);
        uaStart = strchr(ua, ':');
        if(uaStart == NULL){
            uaStart = ua;
        }
        else{
            uaStart++;
        }
        google.SetUserAgent(uaStart);
    }

    curKeyword = 0;
    while((iterrationNum < desiredIterrations) && started){
        if(formData.googleData.isRandomOrder && formData.googleData.totalKeywords){
            curKeyword = rand() % formData.googleData.totalKeywords;
        }
        #ifdef DEBUG
        else if (formData.googleData.totalKeywords == 0){
            log << "TINKLAPIU SARASE NERA!!!" << endLogRec;
            break;
        }
        #endif

        if(!google.LoadMainPage()){
            log << "Neiseina prisijungti prie google." << endLogRec;
        }
        google.search(formData.googleData.keywordsList[curKeyword], formData.googleData.depthMin, formData.googleData.depthMax, formData.googleData.lookMin, formData.googleData.lookMax);

        if(!formData.webSiteData.isRandomOrder){
            curKeyword++;
            if(curKeyword == formData.googleData.totalKeywords){
                curKeyword = 0;
                iterrationNum++;
            }
        }
        else if(formData.webSiteData.isRandomOrder){
            iterrationNum++;
        }
    }
}

void watchYoutube(bool &started){
    unsigned int desiredIterrations, iterrationNum = 0;
    YouTube youtube;

    #ifdef DEBUG
    log <<"watchYoutube()" << endLogRec;
    #endif // DEBUG

    /* YOUTUBE TEST */
    (formData.youtubeRepeatMin == formData.youtubeRepeatMax)?
    (desiredIterrations = formData.youtubeRepeatMin):
    (desiredIterrations = rand()%(formData.youtubeRepeatMax - formData.youtubeRepeatMin) + formData.youtubeRepeatMin);

    #ifdef DEBUG
    log << "Desired youtube itterations: " << desiredIterrations << endLogRec;
    #endif

    if(formData.clientString != NULL){ //if() del visa ko, kad nebutu netiketumu del klaidu programoje
        char *ua = new char[strlen(formData.clientString)];
        char *uaStart = NULL;
        strcpy(ua, formData.clientString);
        uaStart = strchr(ua, ':');
        if(uaStart == NULL){
            uaStart = ua;
        }
        else{
            uaStart++;
        }
        youtube.SetUserAgent(uaStart);
cout << "UA=" << uaStart << endl;
    }
    youtube.LoadMainPage();

    while((iterrationNum < desiredIterrations) && started){
        youtube.WatchRandomVideoFromLink();
        iterrationNum++;
    }
}

DWORD WINAPI Thread_Main(LPVOID pvHDlg){
    HWND hDlg = (HWND)pvHDlg;
    int choice = 0;

    gStopped = false;

    while(gStarted){
        if(formData.isRandom){
            unsigned int random = rand();
            random = rand(); //Kad labiau atsitiktinis butu (srand() ir rand() trukumu workaround)
            choice = random % 5;
        }
        else{
            choice++;
            if(choice == 5){
                choice = 0;
            }
        }
        #ifdef DEBUG
        log << "Pasirinktas veiksmas Thread_Main: " << choice << endLogRec;
        #endif // DEBUG
        switch(choice){
            case 0:
                if((formData.useLinks) && (formData.webSiteData.webSiteList[0][0])){
                    browseWebSite(gStarted);
                }
            break;
            case 1:
                if((formData.useGoogle) && (formData.googleData.keywordsList[0][0])){
                    searchGoogle(gStarted);
                }
            break;
            case 2:
                if(formData.watchYoutube){
                    watchYoutube(gStarted);
                }
            break;
            case 3:
                if(formData.email.isSendOn){
                    unsigned int prepTime = rand()%(formData.email.prepTimeMax - formData.email.prepTimeMin) + formData.email.prepTimeMin;
                    Sleep(prepTime * 1000);
                    sendEmailMessage(formData.email.smtpSettings.server, formData.email.smtpSettings.port, formData.email.smtpSettings.useSSL,
                                     formData.email.smtpSettings.useAuth?formData.email.smtpSettings.userName:NULL,
                                     formData.email.smtpSettings.useAuth?formData.email.smtpSettings.Password:NULL,
                                     formData.email.From , formData.email.To, formData.email.Subject, formData.email.Message);
                }
            break;
            case 4:
                if(formData.email.isRecvOn){
                    unsigned int listTime = rand()%(formData.email.listTimeMax - formData.email.listTimeMin) + formData.email.listTimeMin;
                    unsigned int showTime = rand()%(formData.email.viewTimeMax - formData.email.viewTimeMin) + formData.email.viewTimeMin;
                    checkEmail(formData.email.imapSettings.server, formData.email.imapSettings.port, formData.email.imapSettings.useSSL,
                               formData.email.imapSettings.userName, formData.email.imapSettings.Password, listTime, showTime);
                }
            break;
        }
    }
    SendMessage(hDlg, WM_COMMAND, IDC_BUTTON_START, (LPARAM)GetDlgItem((HWND&)hDlg, IDC_BUTTON_START));
    log << "Uzbaigta." << endLogRec;

    return 0;
}

BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam){
    switch(Message){
        case WM_INITDIALOG:

        return TRUE;
        case WM_COMMAND:
        {
            switch(LOWORD(wParam))
            {
                case IDC_ABOUT_OK:
                    EndDialog(hwnd, IDC_ABOUT_OK);
                break;
            }
        }
        break;
        default: return FALSE;
    }
    return TRUE;
}

BOOL CALLBACK EmailSettingsDlgProc(HWND hDlg, UINT Message, WPARAM wParam, LPARAM lParam){
    switch(Message){
        case WM_INITDIALOG:
            initEmailSettingsDialog(hDlg); //Istraukia ir atvaizduoja nustatymus is formData.
        return TRUE;
        case WM_COMMAND:
        {
            switch(LOWORD(wParam))
            {
                case IDC_BUTTON_SET_OK:
                    GetDlgItemText(hDlg, IDC_EDIT_SET_SMTP_SERVER, formData.email.smtpSettings.server, MAX_DOMAIN_NAME_LENGTH);
                    formData.email.smtpSettings.port = GetDlgItemInt(hDlg, IDC_EDIT_SET_SMTP_PORT, NULL, FALSE);
                    GetDlgItemText(hDlg, IDC_EDIT_SET_SMTP_USER, formData.email.smtpSettings.userName, MAX_USERNAME_LENGTH);
                    GetDlgItemText(hDlg, IDC_EDIT_SET_SMTP_PASSWORD, formData.email.smtpSettings.Password, MAX_PASSWORD_LENGTH);
                    formData.email.smtpSettings.useSSL = SendDlgItemMessage(hDlg, IDC_CHECKBOX_SET_SMTP_SSL, BM_GETCHECK, 0, 0);
                    formData.email.smtpSettings.useAuth = SendDlgItemMessage(hDlg, IDC_CHECKBOX_SET_SMTP_AUTH_RQ, BM_GETCHECK, 0, 0);

                    GetDlgItemText(hDlg, IDC_EDIT_SET_IMAP_SERVER, formData.email.imapSettings.server, MAX_DOMAIN_NAME_LENGTH);
                    formData.email.imapSettings.port = GetDlgItemInt(hDlg, IDC_EDIT_SET_IMAP_PORT, NULL, FALSE);
                    GetDlgItemText(hDlg, IDC_EDIT_SET_IMAP_USER, formData.email.imapSettings.userName, MAX_USERNAME_LENGTH);
                    GetDlgItemText(hDlg, IDC_EDIT_SET_IMAP_PASSWORD, formData.email.imapSettings.Password, MAX_PASSWORD_LENGTH);
                    formData.email.imapSettings.useSSL = SendDlgItemMessage(hDlg, IDC_CHECKBOX_SET_IMAP_SSL, BM_GETCHECK, 0, 0);

                    GetDlgItemText(hDlg, IDC_EDIT_SET_EMAIL, formData.email.From, MAX_EMAIL_ADDRESS_LENGTH);

                    EndDialog(hDlg, IDC_BUTTON_SET_OK);
                break;
                case IDC_BUTTON_SET_CANCEL:
                    EndDialog(hDlg, IDC_BUTTON_SET_CANCEL);
                break;
                case IDC_CHECKBOX_SET_SMTP_SSL:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        SetDlgItemInt(hDlg, IDC_EDIT_SET_SMTP_PORT, 25, FALSE);
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        SetDlgItemInt(hDlg, IDC_EDIT_SET_SMTP_PORT, 465, FALSE);
                    }
                }
                break;
                case IDC_CHECKBOX_SET_SMTP_AUTH_RQ:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        SendDlgItemMessage(hDlg, IDC_EDIT_SET_SMTP_USER, EM_SETREADONLY, TRUE, 0);
                        SendDlgItemMessage(hDlg, IDC_EDIT_SET_SMTP_PASSWORD, EM_SETREADONLY, TRUE, 0);
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        SendDlgItemMessage(hDlg, IDC_EDIT_SET_SMTP_USER, EM_SETREADONLY, FALSE, 0);
                        SendDlgItemMessage(hDlg, IDC_EDIT_SET_SMTP_PASSWORD, EM_SETREADONLY, FALSE, 0);
                    }
                }
                break;
                case IDC_CHECKBOX_SET_IMAP_SSL:
                {
                    if(SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED){
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_UNCHECKED, 0);
                        SetDlgItemInt(hDlg, IDC_EDIT_SET_IMAP_PORT, 0, FALSE);
                    }
                    else{
                        SendMessage((HWND)lParam, BM_SETCHECK, BST_CHECKED, 0);
                        SetDlgItemInt(hDlg, IDC_EDIT_SET_IMAP_PORT, 993, FALSE);
                    }
                }
                break;
            }
        }
        break;
        case WM_CLOSE:
        {
            EndDialog(hDlg, IDC_BUTTON_SET_CANCEL); // Ar taip korektiska?
        }
        break;
        default: return FALSE;
    }
    return TRUE;
}

void initEmailSettingsDialog(HWND hDlg){
    SetDlgItemText(hDlg, IDC_EDIT_SET_SMTP_SERVER, formData.email.smtpSettings.server);
    SetDlgItemText(hDlg, IDC_EDIT_SET_SMTP_USER, formData.email.smtpSettings.userName);
    SetDlgItemText(hDlg, IDC_EDIT_SET_SMTP_PASSWORD, formData.email.smtpSettings.Password);
    SetDlgItemInt(hDlg, IDC_EDIT_SET_SMTP_PORT, formData.email.smtpSettings.port, FALSE);

    SetDlgItemText(hDlg, IDC_EDIT_SET_IMAP_SERVER, formData.email.imapSettings.server);
    SetDlgItemText(hDlg, IDC_EDIT_SET_IMAP_USER, formData.email.imapSettings.userName);
    SetDlgItemText(hDlg, IDC_EDIT_SET_IMAP_PASSWORD, formData.email.imapSettings.Password);
    SetDlgItemInt(hDlg, IDC_EDIT_SET_IMAP_PORT, formData.email.imapSettings.port, FALSE);

    SetDlgItemText(hDlg, IDC_EDIT_SET_EMAIL, formData.email.From);

    if(formData.email.smtpSettings.useSSL){
        SendDlgItemMessage(hDlg, IDC_CHECKBOX_SET_SMTP_SSL, BM_SETCHECK, BST_CHECKED, 0);
    }
    if(formData.email.smtpSettings.useAuth){
        SendDlgItemMessage(hDlg, IDC_CHECKBOX_SET_SMTP_AUTH_RQ, BM_SETCHECK, BST_CHECKED, 0);
    }
    else{
        SendDlgItemMessage(hDlg, IDC_EDIT_SET_SMTP_USER, EM_SETREADONLY, TRUE, 0);
        SendDlgItemMessage(hDlg, IDC_EDIT_SET_SMTP_PASSWORD, EM_SETREADONLY, TRUE, 0);
    }
    if(formData.email.imapSettings.useSSL){
        SendDlgItemMessage(hDlg, IDC_CHECKBOX_SET_IMAP_SSL, BM_SETCHECK, BST_CHECKED, 0);
    }
}

bool inputCheck(HWND hDlg){
    char String[MAX_MESSAGE_STRING_LENGTH];
    char Error[MAX_SHORT_MESSAGE_STRING_LENGTH];
    bool foundError = false;

    LoadString(GetModuleHandle(NULL), IDS_ERROR, Error, MAX_SHORT_MESSAGE_STRING_LENGTH);
    if((formData.googleData.depthMin > formData.googleData.depthMax) ||
       (formData.webSiteData.depthMin > formData.webSiteData.depthMax) ||
       (formData.googleData.lookMin > formData.googleData.lookMax) ||
       (formData.webSiteData.lookMin > formData.webSiteData.lookMax) ||
       (formData.googleData.repeatMin > formData.googleData.repeatMax) ||
       (formData.webSiteData.repeatMin > formData.webSiteData.repeatMax) ||
       (formData.youtubeRepeatMin > formData.youtubeRepeatMax) ||
       (formData.email.listTimeMin > formData.email.listTimeMax) ||
       (formData.email.prepTimeMin > formData.email.prepTimeMax) ||
       (formData.email.viewTimeMin > formData.email.viewTimeMax))
    {
        LoadString(GetModuleHandle(NULL), IDS_MINGRTMAX, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.googleData.depthMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_GOOGLE_DEPTH_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.webSiteData.depthMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_WEB_DEPTH_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.googleData.lookMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_GOOGLE_VIEW_TOO_SHORT, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.webSiteData.lookMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_WEB_VIEW_TOO_SHORT, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.googleData.repeatMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_GOOGLE_REPEAT_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.webSiteData.repeatMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_WEB_REPEAT_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.youtubeRepeatMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_YOUTUBE_REPEAT_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.email.listTimeMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_EMAIL_LIST_TIME_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.email.prepTimeMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_EMAIL_PREP_TIME_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }
    else if(formData.email.viewTimeMin < 0){
        LoadString(GetModuleHandle(NULL), IDS_ERR_EMAIL_VIEW_TIME_TOO_SMALL, String, MAX_MESSAGE_STRING_LENGTH);
        foundError = true;
    }

    if(foundError){
        MessageBoxA(hDlg, String, Error, MB_OK | MB_ICONERROR);
    }

    return !foundError;
}
