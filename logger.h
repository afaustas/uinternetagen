#ifndef __LOG_H
#define __LOG_H

#include <fstream>
#include <string>
#include <time.h>
#include <windows.h>

using namespace std;

class logger
{
public:
    logger(HWND hWnd = NULL):hWnd_(hWnd), insertDateTime(true){};
    ~logger(){file.close();}
    friend logger & operator<<(logger &l, const string &s);
    friend logger & operator<<(logger &l, logger &(&fptr)(logger &l));
    friend logger & operator<<(logger &l, const int &i);
    friend logger & endLogRec(logger &l);
    void setWindowHandle(HWND hWnd){hWnd_ = hWnd;};
    void setFileName(string fileName);
    void stopFileLogging();
    bool insertDateTime;
    bool isFileLoggingOn = false; //TODO: reiketu perkelt i private ir naudoti getter'i
private:
    HWND hWnd_;
    string fileName_;
    ofstream file;
    char dateBuffer [80];
    void getDate();
    void printString(const string &s);
};

logger & operator<<(logger &l, const string &s);
logger & operator<<(logger &l, logger &(&fptr)(logger &l));
logger & operator<<(logger &l, const int &i);
logger & endLogRec(logger &l);

/* Loginimo funkcijai */
#define LOG_LINES_PER_PAGE       50

typedef struct logData_{
                         unsigned char myIP[6]; //IPv6 kad tilptu
                         unsigned char srvIP[6];
                         unsigned long int bytesSent;
                         unsigned long int bytesRecv;
                         unsigned int myPort;
                         unsigned int srvPort;
                         string link;
                       } LOGDATA;

void writeLogDataLine(LOGDATA *logData, fstream *CSVfile, bool linesCountReset = false);
#endif //__LOG_H
