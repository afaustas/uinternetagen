#include <afxres.h>

//Dialogai
#define IDD_ABOUT           101
#define IDD_MAIN            102
#define IDD_MAIL_SETTINGS   103

//Pagrindinis dialogas
#define IDS_MINGRTMAX                       200
#define IDS_ERR_WEB_DEPTH_TOO_SMALL         201
#define IDS_ERR_WEB_VIEW_TOO_SHORT          202
#define IDS_WARN_WEB_VIEW_TOO_LONG          203
#define IDS_ERR_WEB_REPEAT_TOO_SMALL        204
#define IDS_ERR_GOOGLE_DEPTH_TOO_SMALL      205
#define IDS_ERR_GOOGLE_VIEW_TOO_SHORT       206
#define IDS_WARN_GOOGLE_VIEW_TOO_LONG       207
#define IDS_ERR_GOOGLE_REPEAT_TOO_SMALL     208
#define IDS_ERR_YOUTUBE_REPEAT_TOO_SMALL    209
#define IDS_ERR_BAD_FILE_NAME               210
#define IDS_ERROR                           211
#define IDS_START                           212
#define IDS_STOP                            213
#define IDS_ERR_EMAIL_LIST_TIME_TOO_SMALL   214
#define IDS_ERR_EMAIL_PREP_TIME_TOO_SMALL   215
#define IDS_ERR_EMAIL_VIEW_TIME_TOO_SMALL   216

#define IDC_EDIT_SITE_LIST                  0
#define IDC_RADIOBUTTON_CH_SITE_RANDOM      1
#define IDC_RADIOBUTTON_CH_SITE_SEQ         2
#define IDC_EDIT_SITE_DEPTH_MIN             3
#define IDC_EDIT_SITE_DEPTH_MAX             4
#define IDC_EDIT_SITE_LOOK_MIN              5
#define IDC_EDIT_SITE_LOOK_MAX              6

#define IDC_EDIT_GOOGLE_KEYWORDS_LIST       7
#define IDC_RADIOBUTTON_CH_KEYWORDS_RANDOM  8
#define IDC_RADIOBUTTON_CH_KEYWORDS_SEQ     9
#define IDC_EDIT_GOOGLE_DEPTH_MIN           10
#define IDC_EDIT_GOOGLE_DEPTH_MAX           11
#define IDC_EDIT_GOOGLE_LOOK_MIN            12
#define IDC_EDIT_GOOGLE_LOOK_MAX            13

#define IDC_EDIT_STATUS                     14

#define IDC_CHECKBOX_YOUTUBE                15
#define IDC_CHECKBOX_GOOGLE                 24
#define IDC_CHECKBOX_WEB                    25

#define IDC_BUTTON_SAVE_SETTINGS            16
#define IDC_BUTTON_ABOUT                    17
#define IDC_BUTTON_START                    18

#define IDC_CHECKBOX_WRITE_TO_FILE          19
#define IDC_EDIT_FILENAME                   20

#define IDC_GROUPBOX_WEB_BROWSING           22
#define IDC_GROUPBOX_GOOGLE                 23

#define IDC_COMBOBOX_CLIENT                 26
#define IDC_EDIT_SITE_REPEAT_MIN            27
#define IDC_EDIT_SITE_REPEAT_MAX            28
#define IDC_EDIT_GOOGLE_REPEAT_MIN          29
#define IDC_EDIT_GOOGLE_REPEAT_MAX          30
#define IDC_GROUPBOX_YOUTUBE                31
#define IDC_EDIT_YOUTUBE_REPEAT_MIN         32
#define IDC_EDIT_YOUTUBE_REPEAT_MAX         33
#define IDC_EDIT_TSV_FILENAME               34
#define IDC_BUTTON_TSV_BROWSE               35
#define IDC_CHECKBOX_TSV_WRITE_TO_FILE      36

#define IDC_GROUPBOX_EMAIL                  37
#define IDC_CHECKBOX_EMAIL_SMTP_EN          38
#define IDC_EDIT_EMAIL_SUBJECT              39
#define IDC_EDIT_EMAIL_TO                   40
#define IDC_CHECKBOX_EMAIL_READ_EN          41
#define IDC_GROUPBOX_CSERVER                42
#define IDC_GROUPBOX_CCLIENT                43
#define IDC_BUTTON_EMAIL_SETTINGS           44
#define IDC_EDIT_EMAIL_PREP_MIN             45
#define IDC_EDIT_EMAIL_PREP_MAX             46
#define IDC_EDIT_EMAIL_LIST_TIME_MIN        47
#define IDC_EDIT_EMAIL_LIST_TIME_MAX        48
#define IDC_EDIT_EMAIL_VIEW_TIME_MIN        49
#define IDC_EDIT_EMAIL_VIEW_TIME_MAX        50
#define IDC_EDIT_CSERVER                    51
#define IDC_EDIT_CCLIENT_QUERY_LEN_MIN      52
#define IDC_EDIT_CCLIENT_QUERY_LEN_MAX      53
#define IDC_EDIT_CQUERY_FREQ_MIN            54
#define IDC_EDIT_CQUERY_FREQ_MAX            55
#define IDC_CHECKBOX_CCLIENT_ON             56
#define IDC_RADIOBUTTON_CCS_TCP             57
#define IDC_RADIOBUTTON_CCS_UDP             58
#define IDC_EDIT_CSERVER_PORT               59
#define IDC_EDIT_CSERVER_ANSWER_MIN         60
#define IDC_EDIT_CSERVER_ANSWER_MAX         61
#define IDC_CHECKBOX_CSERVER_ON             62
#define IDC_EDIT_EMAIL_CONTENT              63

// About dialogas
#define IDC_ABOUT_GROUPBOX_INFO             301
#define IDC_ABOUT_OK                        302
#define IDC_ABOUT_PICTURE                   303
#define IDC_CHECKBOX_RANDOM                 304

// Mail settings dialogas
#define IDC_GROUPBOX_SET_SMTP               401
#define IDC_GROUPBOX_SET_IMAP               402
#define IDC_BUTTON_SET_OK                   403
#define IDC_BUTTON_SET_CANCEL               404
#define IDC_EDIT_SET_SMTP_SERVER            405
#define IDC_EDIT_SET_SMTP_PORT              406
#define IDC_CHECKBOX_SET_SMTP_SSL           407
#define IDC_CHECKBOX_SET_SMTP_AUTH_RQ       408
#define IDC_EDIT_SET_SMTP_USER              409
#define IDC_EDIT_SET_SMTP_PASSWORD          410
#define IDC_EDIT_SET_IMAP_SERVER            411
#define IDC_EDIT_SET_IMAP_PORT              412
#define IDC_CHECKBOX_SET_IMAP_SSL           413
#define IDC_EDIT_SET_IMAP_USER              414
#define IDC_EDIT_SET_IMAP_PASSWORD          415
#define IDC_EDIT_SET_EMAIL                  416
