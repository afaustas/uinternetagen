#include "WebSite.h"

/// //////////////////////////////////////////////////////////////////////////
/// TODO:
///  1. Saknines direktorijos ('/') teisingas interpretavimas - funkcija
///  2. Grizimas atgal i ankstesne direktorija ("..") - funkcija
///  3. Access Violation jungiantis spresti
///  4. Downloads tvarkyt.
///  5. Nustatyti, kad download be dvigubo jungimosi!
/// //////////////////////////////////////////////////////////////////////////

extern logger log;
extern fstream g_TSV_file;

static char *g_firstStack = NULL;

WebSite::WebSite(){
    linkList = new char*[MAX_LINKS];
    for (size_t i = 0; i < MAX_LINKS; i++) {
      linkList[i] = new char[MAX_LINK_LENGTH];
    }
    srand(time(NULL));
    SetDefaultUserAgent();
}

WebSite::~WebSite(){
   for (size_t i = MAX_LINKS; i > 0; ) {
        delete[] linkList[--i];
   }
   delete[] linkList;
   sc.EndConnection();
}

bool WebSite::httpQuery(char query[]){
    sc.httpQuery(sck, query);
    return true; ///Kolkas visad atseit gerai
}

bool WebSite::SetName(char SiteName[]){
    char domainName[MAX_DOMAIN_NAME_LENGTH];
    char protocolName[MAX_PROTOCOL_NAME_LENGTH];

    #ifdef DEBUG
    log << "Pasirinktas tinklapis: " << SiteName << ". Nustatomas protokolas: ";
    #endif
    isSecureConnection = false;
    sc.EndConnection(); ///Jei prisijunges (GAL TAI VERTETU PATIKRINTI???!!!.

    Name = ""; //Prideta 20142829
    CurrentSiteName = ""; //Prideta 20140829

    if(!extractDomainName(SiteName, domainName, MAX_DOMAIN_NAME_LENGTH)){
        strcpy(domainName, SiteName);
    }

    if(extractProtocolName(SiteName, protocolName, MAX_PROTOCOL_NAME_LENGTH)){
        if(stricmp(protocolName, "https")){
            if(stricmp(protocolName, "http")){
                ///Nezinomas protokolas
                #ifdef DEBUG
                log << "NEZINOMAS PROTOKOLAS ";
                #endif // DEBUG
                return true; //atseit praejau ir turiu vel link'u sarasa, is kurio renkuos kita linka. TODO: o kas jei tam sarase yra tik vienintelis - sis linkas? Begalinis ciklas?!
            }
            else{ //tai http
                if(!sc.SetConnection(domainName)) return false;
                CurrentSiteName = "http://";
                #ifdef DEBUG
                log << "HTTP!";
                #endif
            }
        }
        else{ //tai https
            if(!sc.SetConnection(domainName, true)) return false;
            isSecureConnection = true;
            CurrentSiteName = "https://";
            #ifdef DEBUG
            log << "HTTPS!";
            #endif
        }
        #ifdef DEBUG
        log << endLogRec;
        #endif
    }
    else{ //Protokolas nenurodytas, tad laikau, kad http protokolas 80 portas.
        string str(domainName);
        str += ":http";
        if(!sc.SetConnection((char*)str.c_str())) return false; //TODO: turetu priimti const char
        CurrentSiteName = "http://";
        #ifdef DEBUG
        log << "Protokolas nenurodytas. Laikoma, kad tai HTTP." << endLogRec;
        #endif
    }

    ///sc.reconnect(); //paskutinir arg neturi prasmes 08/05
    Name = domainName;
    ///originalName = Name; - nuo seniai

    CurrentSiteName += domainName;

    return true;
}

bool WebSite::LoadPage(const char link[], char *newPath, int newPathLength){
    char redirect[MAX_LINK_LENGTH] = "";
    string query;
    char domainName[MAX_DOMAIN_NAME_LENGTH];
    char pathName[MAX_LINK_LENGTH];
    char protocolName[10] = "";

    #ifdef DEBUG
    log << " ---> Kraunamas puslapis... <---" << endLogRec;
    log << "Nuoroda (iprastai sudaryta Browse() metode): " << link << endLogRec;
    #endif
    logData.link = link; //Nuoroda rasau i log ir tsv

    if(!extractDomainName(link, domainName, MAX_DOMAIN_NAME_LENGTH)){ ///Jei domeno vardas nenurodytas, reiskias jis yra esamas
        //Domeno vardas nuorodoje nenurodytas, taigi domenas nesikeicia
        strcpy(domainName, Name.c_str());
        if(extractProtocolName(link, protocolName, 10)){ ///TODO: pala, kaip gali buti nurodytas protokolas, jei nenurodytas domenas????
            system("pause"); ///0827
            if(strcmp(protocolName, "http") && strcmp(protocolName, "https")){ ///<-- iseina, nes nenurod. protokolas. Nustatyti default http kad butu.
                return false; //Nepalaikomas protokolas.
            }
        }
        //else Protokolas nenurodytas - pagal default laikau, kad tai http://
    }
    else if(strcmp(Name.c_str(), domainName)){ ///0827 is if() pakeista i else if(), nes tai nieko nekeicia
        //Domeno vardas is linko nesutampa su dabartiniu, tad reikia pakeisti
        #ifdef DEBUG
        log << "Keiciamas vardas i: " << domainName;
        #endif
        if(!SetName(domainName)){
            #ifdef DEBUG
            log << " -NEPAVYKO PAKEISTI" << endLogRec;
            #endif
            sc.EndConnection();
            return false;   ///????????????????????? TODO: ar tikrai?  Grazinti nauja adresa reikia arg. 0828 manau tikrai return false;
        }
        #ifdef DEBUG
        else log << " -SEKMINGAI PAKEISTAS" << endLogRec;
        #endif
    }
    //else Domenas link'e nurodytas, bet yra toks pats kaip ir dabartinis, tad nieko keisti nereikia

    extractPathName(link, pathName, MAX_LINK_LENGTH); //Is link'o istraukiamas tik kelias, pvz: http://www.afaustas.lt/an/pc/ibm/t20.html --> /an/pc/ibm/

lmp_begin:

    do{
        //Suformuoju uzklausa, kad gauciau pradini puslapi
        //Pvz:  "GET / HTTP/1.1\r\n"
        //      "Host: www.youtube.com\r\n"
        //      "X-Client-Data: CNO1yQEIjLbJAQiltskBCKm2yQEIxLbJAQiehsoBCLiIygEI7IjKAQ==\r\n\r\n";
        //      ...
        query  = "GET ";
        query += pathName; //Pvz.: "/fr/";
        query += " HTTP/1.1\r\nHost: ";
        query += domainName; //Pvz.: www.afaustas.lt
        query += "\r\nConnection: close\r\n";
        //query += "Cache-Control: max-age=0\r\n";
        query += "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*|*;q=0.8\r\n";
        query += "User-Agent: ";
        query += UserAgent;
        query += "Accept-Encoding: gzip,deflate,sdch\r\n";
        query += "Accept-Language: en-US,en;q=0.8";
        query += "\r\n\r\n";
        ///query += "Cookie: __qfp....................(daug visokiu nesamoniu - sausainio duomenys)................"
//cout << "stop"; system("pause");
        if (!sc.reconnect()) return true; //Jei nepavyko prisijungti (20140820) - laikau, kad viskas gerai, nors istiesu ne. Taip Browse() metode pereinu prie kito link'o
        ///TODO: ^ Jei link'as tik vienas yra sarase, tai gal cia gali uzsiciklint, nes link'u sarasas nepasikeicia, kreipiamasi i si metoda su tais pat
        ///duomenimis dar karta!!!
        ///Galimas sprendimas: ///todo: LinksInList = 0; <-- TODO: pranesti, kad aklaviete, o tada return false;
//cout << "stop"; system("pause");
        logData.myPort = sc.GetIPandPort(logData.myIP, true);  //I log ir tsv rasau savo port ir IP
        logData.srvPort = sc.GetIPandPort(logData.srvIP, false); //I log ir tsv rasau serverio port ir IP
        ///TODO: ^Sita automatiskai turetu atlikineti writeLogLine() fnkcija!

        #ifdef DEBUG
        log << "Uzklausa: \r\n" << query << endLogRec;
        #endif

        logData.bytesSent = query.length(); //I log ir tsv: kiek issiusta baitu

        httpQuery((char*)query.c_str()); //Siunciu uzklausa
        ///printf("Atsakymas: %s\r\n", GetAnswer()); <-- Iprastai labai ilgas

        if(sc.answer == NULL) {
            //Jei nepavyko gauti ats is serverio, tai teigiama, kad viskas gerai.
            ///TODO: jei "return true", tai link'u sarasas nesikeicia. Tad jei sarase tik vienas linkas, tai kvieciamas tas pats.
            ///Jei rezultatas nesikeicia, uzsiciklinti turetu

            ///todo: LinksInList = 0; <-- TODO: pranesti, kad aklaviete, o tada return false;
            return true;
            ///continue - vienas continue tik irgi nevariantas: butu bandoma dar karta daug kartu, gal ir be galo daug, jei neimanoma;
        }

        logData.bytesRecv = sc.GetLengthOfAnswer();
        writeLogDataLine(&logData, &g_TSV_file);

        //Tikrinu, ar nera nukreipimo kitur:
        char *headerEnd = NULL;
        char *location = NULL;
        char domain[MAX_DOMAIN_NAME_LENGTH];
        char proto[10]; //Cia turi buti "http"

        #ifdef DEBUG
        /* Headeris rasomas i log */
        log << "LoadPage() GAUTAS HEADER:" << endLogRec;
        {
            char l[401];
            for(int i=0; (i < 400) && sc.answer[i]; i++){
                l[i] = sc.answer[i];
            }
            l[400] = '\0';
            log << l << endLogRec;
        }
        #endif

        /* Ieskoma header'io pabaiga */
        headerEnd = strstr(sc.answer, "\r\n\r\n");
        if(!headerEnd){
            headerEnd = strstr(sc.answer, "\n\r\n\r"); //Siaip tokia situacija neturetu buti galima pagal spec.
            log << __FILE__": PASTEBETA NESTANDARTINE SITUACIJA (Ne CR-LF, o LF-CR)!. Bet kolkas sekmingai tesiama." << endLogRec;
        }

        if(headerEnd){
            *headerEnd = '\0';
            //cout << sc.answer;
            location = strstr(sc.answer, "\r\nLocation: "); //Location nebuna, jei rezimas yra chunked, kuris pasitaiko reciau. ///20140828 - prideta \r\n, kad nesiskaitytu "Content-Location" - tai visai kas kita
            if(location){
                bool hasProtocol, hasDomainName, isProtocolSupported = false;
                int endPos;
                location += 2; //20140828 prideta, kad praeiti \r\n
                location += sizeof("Location: ") - 1;
                for(endPos = 0; location[endPos] != '\r'; endPos++);
                location[endPos] = '\0';
                strcpy(redirect, location);
                location[endPos] = '\r';
                #ifdef DEBUG
                log << "Rastas LOCATION, tad vykdomas nukreipimas (redirect): " << redirect << endLogRec;
                #endif
                free(sc.answer);
                sc.answer = NULL;
                hasProtocol = extractProtocolName(redirect, proto, 10);
                hasDomainName = extractDomainName(redirect, domain, MAX_DOMAIN_NAME_LENGTH);
                isProtocolSupported = !(strcmp(proto, "http") && strcmp(proto, "https"));
                #ifdef DEBUG
                log << "PROTO: "<< proto <<", domain: " << domain << endLogRec;
                #endif
                if(hasDomainName && hasProtocol && isProtocolSupported){
                    string url;
                    url = proto;
                    url += "://";
                    url += domain;
                    sc.EndConnection();
                    SetName((char*)url.c_str()); //Kitas domenas
                    #ifdef DEBUG
                    log << "LOCATION url: " << url.c_str() << endLogRec;
                    #endif
                    extractPathName(redirect, pathName, MAX_LINK_LENGTH);
                    strcpy(domainName, domain); //Eilute prideta taisant klaida http://www.ji24.lt, 20140824
                }
                else if(hasProtocol && !isProtocolSupported){
                    #ifdef DEBUG
                    log << "Protokolas nepalaikomas. " << endLogRec;
                    #endif
                    LinksInList = 0; //Atseit browser'io lange pranesimas "protokolas nepalaikomas" ir nuorodu nebera - aklaviete
                    return false;
                }
                else{ //protokolas nenurodytas, o tai reiskia, kad ir domenas nenurodytas (iskart kelias)
                    //sc.EndConnection();
                    #ifdef DEBUG
                    log << "Pastebejau, kad nurodytas tik kelias: ";
                    #endif // DEBUG
                    extractPathName(redirect, pathName, MAX_LINK_LENGTH);
                    #ifdef DEBUG
                    log << pathName << endLogRec;
                    #endif // DEBUG
                }
                goto lmp_begin; //Buvo location, tad situacija pasikeite. Kartoju uzklausa...
            }
            else{
                redirect[0] = '\0';
            }
            *headerEnd = '\r';
        }
        //else headerio pabaiga nerasta - kazkas netaip!!!
    }
    while(redirect[0]);

    /* Tikrinu mime - ar tai tikrai puslapis? */
    {
        size_t ctBegin;
        size_t ctEnd;
        size_t ctComma;
        string mime;
        string answer;
        char toFind[] = "content-type: ";
        answer = sc.answer;
        string tmpAnswer = sc.answer;

        std::transform(tmpAnswer.begin(), tmpAnswer.end(), tmpAnswer.begin(), ::tolower);
        ctBegin = tmpAnswer.find(toFind);
        if(ctBegin != string::npos){
            ctBegin += sizeof("content-type: ") - 1;
            ctEnd = tmpAnswer.find("\r\n", ctBegin);
            ctComma = tmpAnswer.find(";", ctBegin);
            if(ctEnd == string::npos){
                ctEnd = tmpAnswer.find("\n\r");
                if(ctEnd == string::npos) return false; //Header'io klaida - nepilnas header'is
            }
            if((ctBegin < ctComma)&&(ctComma < ctEnd)){
                mime = tmpAnswer.substr(ctBegin, ctComma - ctBegin);
            }
            else{
                mime = tmpAnswer.substr(ctBegin, ctEnd - ctBegin);
            }
            #ifdef DEBUG
            log << "MIME: " << mime << ctEnd << " " << ctBegin << endLogRec;
            #endif // DEBUG

            if(mime != "text/html"){
                #ifdef DEBUG
                log << "MIME NELYGUS TEXT/HTML!!! Pradedamas bin failo siuntimas..." << endLogRec;
                #endif
                LinksInList = 0;
                fooDownload((char*)link);
                return false;
            }
        }
        else return false; //Netiketa klaida - atsakyme privalo buti nurodytas mime tipas.
    }

    /*if(newPath)*/{ ///Jei argumentas nurodytas (pointer newPath != NULL) - siaip privalo buti nurodytas!
        string fullPath;
        fullPath = "http://"; ///Palaikomas tik sis protokolas. TODO: O tai kaip su https?
        fullPath += domainName;
        //Redirectinimas gali vykti ne butinai i direktorija, pvz, "Location: fr/",
        //bet ir i faila, pvz, "Location: fr/contacts.php". Tuo atveju, kai redirect'inama
        //i faila, as turiu pakeisti darbine direktorija.
        #ifdef DEBUG
        log << "Istraukiama direktorija, kai pathName=" << pathName << endLogRec;
        #endif
        fullPath += extractDirectoryName(pathName);
        #ifdef DEBUG
        log << "Path: " << fullPath << endLogRec;
        #endif
        if(strlen(fullPath.c_str()) < newPathLength + 1){
            int len;
            strcpy(newPath, fullPath.c_str());
            len = strlen(newPath);
            if(newPath[len - 1] != '/'){
                newPath[len] = '/';
                newPath[len+1] = '\0';
            }
        }
    }

    /* Surenku linkus */
    {
        char htmlValue[50];
        unsigned long int curPosition = 0, curLinkNr = 0;
        do{
            curPosition = TagParser("a", "href", linkList[curLinkNr++], MAX_LINK_LENGTH, sc.answer, sc.GetLengthOfAnswer(), curPosition);
            removeNewLinesAndChunkData(linkList[curLinkNr - 1]);
            #ifdef DEBUG
           // printf("%d %s\n\r", curLinkNr-1, linkList[curLinkNr - 1]);
           /*{
               char protokolas [10];
               char domenas[256];
               char kelias[1024];
               extractProtocolName(linkList[curLinkNr - 1], protokolas, 10);
               extractDomainName(linkList[curLinkNr - 1], domenas, 256);
               extractPathName(linkList[curLinkNr - 1], kelias, 1024);
               cout  << "Protokolas: " << protokolas << endl;
               cout  << "Domenas: " << domenas << endl;
               cout  << "Kelias: " << kelias << endl <<endl;
           }*/
           #endif
        }
        while((curPosition < sc.GetLengthOfAnswer()) && (curLinkNr < MAX_LINKS));
        ///LinksInList = max(curLinkNr, LinksInList); - jei kiekviena vis naujame
        LinksInList = curLinkNr - 1;
        #ifdef DEBUG
        log << "NUORODU SARASE: " << LinksInList << endLogRec;
        #endif
    }

    log << "Uzkraunu pav:" << endLogRec;
    fooLoadFiles(newPath, sc.answer, sc.GetLengthOfAnswer(), "img", "src");
    log << "Uzkraunu JS:" << endLogRec;
    fooLoadFiles(newPath, sc.answer, sc.GetLengthOfAnswer(), "script", "src");
    log << "Uzkraunu CSS:" << endLogRec;
    fooLoadFiles(newPath, sc.answer, sc.GetLengthOfAnswer(), "link", "href", "rel", "stylesheet");

    ///free(sc.answer);
    return true;
}

unsigned long int WebSite::TagParser(const char htmlTag[], const char htmlAttrib[], char htmlValue[], const unsigned int maxHtmlValueLen,
                                     const char data[], const unsigned long int dataLength, const unsigned long int curPosition){

    unsigned long int curByte, x;
    bool found = false;

    for(curByte = curPosition; (curByte < dataLength) && (!found); curByte++){
        if(data[curByte] == '<'){
            while((data[curByte] == ' ') && (curByte < dataLength) && (data[curByte] != '>')) curByte++;
            if(!memicmp(&data[++curByte], htmlTag, strlen(htmlTag))){
                            /*{
                                cout << endl << "TAG: ";
                                for(int x=curByte; x < curByte + 30; x++) putchar(data[x]);
                                system("pause");
                            }*/

                curByte += strlen(htmlTag) + 1;
                while((data[curByte] != '>') && (!found) && (curByte < dataLength)){
                    while((data[curByte] == ' ') && (curByte < dataLength)) curByte++;
                    if(!memicmp(&data[curByte], htmlAttrib, strlen(htmlAttrib))){
                            /*{
                                cout << endl << "ATTRIB: ";
                                for(int x=curByte; x < curByte + 200; x++) putchar(data[x]);
                                system("pause");
                            }*/
                        while((data[curByte] != '=') && (data[curByte] != '>') && (curByte < dataLength) && (data[curByte] != '>')) curByte++;
                        if(data[curByte] == '='){
                            while((data[curByte] != '"') && (data[curByte] != '\'') && (curByte < dataLength)) curByte++;
                            if((data[curByte] == '"') || (data[curByte] == '\'')){
                                curByte++;
                                x = 0;
                                while((data[curByte] != '"') && (data[curByte] != '\'') && (curByte < dataLength) && (x < maxHtmlValueLen)){
                                    htmlValue[x++] = data[curByte++];
                                }
                                htmlValue[(x == maxHtmlValueLen)?(x-1):x] = 0; //Kad isvengti rasymo uz ribu. |TIKRINT: sal?true:false
                                found = true;
                            }
                        }
                    }
                    else{
                        curByte++;
                        while((data[curByte] != ' ') && (curByte < dataLength) && (data[curByte] != '>')) {
                                curByte++;
                        }
                    }
                }
            }
        }
    }
    return curByte;
}

bool WebSite::extractProtocolName(const char link[], char *protocolName, int maxProtocolNameLength){
    const char protSeparator[] = "://";
    char *protoEnd;

    protocolName[0] = '\0';
    protoEnd = strstr(link, protSeparator);
    if(protoEnd){
        if(protoEnd - link < maxProtocolNameLength){
            strncpy(protocolName, link, protoEnd - link);
            protocolName[protoEnd - link] = '\0';
            return true;
        }
    }
    return false;
}

bool WebSite::extractDomainName(const char link[], char *domainName, int maxDomainLength){
    signed int pos, length;
    const char protSeparator[] = "://";
    bool tikriausia_domenas = false;
    const char *domainNameStart = NULL, *domainNameEnd = NULL;

    domainName[0] = '\0';

    /*  Pasitaiko, kad linkai, kuriuose yra nurodytas domenas nebutinai susideda is pradzioje nurodomo protokolo, kaip
        pavyzdziui "http://www.afaustas.lt/an/index.php" . Taciau pastebejau, kad pasitaiko ir tokiu atveju:
        "//s.ytimg.com/yts/jsbin/html5player-lt_LT-vflEkvKvu/html5player.js" . Siuo atveju domenas yra "s.ytimg.com"
        Tad funkcija papildau workaround'u. */
    if((strlen(link) > 5) && (link[0] == '/') && (link[1] == '/')){
        char *domainNameEnd = NULL;
        strncpy(domainName, &link[2], maxDomainLength);
        domainNameEnd = strchr(domainName, '/');
        if(domainNameEnd){
            *domainNameEnd = '\0';
        }
        tikriausia_domenas = true;
    }

    /* Iprastas domeno isskyrimo algoritmas, pvz, kai http://www.afaustas.lt/an/index.php ==> "www.afaustas.lt" */
     else if(strlen(link) > sizeof(protSeparator)){
        domainNameStart = strstr(link, protSeparator) + sizeof(protSeparator) - 1;
        if(domainNameStart - sizeof(protSeparator) + 1 == 0) return false; ///Buvo: domainNameStart = link;, taciau, jei nera nurodyta protokolas, tai nuorodoje yra failas, o ne domenas.
        domainNameEnd = strchr(domainNameStart, '/');

        if(domainNameEnd == NULL) domainNameEnd = link + strlen(link);

        if(domainNameEnd - domainNameStart + 1 < maxDomainLength){
            length = domainNameEnd - domainNameStart  + 1;
        }
        else{
            length = maxDomainLength;
        }

        strncpy(domainName, domainNameStart, length);
        length ? (domainName[length - 1] = '\0') : (domainName[0] = '\0');

        //Tikrinu, ar tikrai domenas (na patikrinimas menkas, bet is esmes to uztenka (bent kolkas)
        for(int x = 1; (x < strlen(domainName)) && (!tikriausia_domenas); x++){
            if(domainName[x] == '.') tikriausia_domenas = true;
        }
        if(!tikriausia_domenas) domainName[0] = '\0';
    }

    return tikriausia_domenas;
}

bool WebSite::extractPathName(const char link[], char *pathName, int maxPathLength){

    char domain[MAX_DOMAIN_NAME_LENGTH]; //EPN_MAX_DOMAIN_LEN];
    char *domainStart = NULL;
    int pathStartPosition;
    bool isPathFound = false;

    strcpy(pathName, "/");

    if(extractDomainName(link, domain, MAX_DOMAIN_NAME_LENGTH)){ //Jei domenas nurodytas linke
        domainStart = strstr(link, domain);
        if(domainStart){
            pathStartPosition = domainStart - link;
            for(;(pathStartPosition < strlen(link))&&(link[pathStartPosition] != '/'); pathStartPosition++);
            if(pathStartPosition == strlen(link)){
                //Kelias nenurodytas. Pagal nutylejima pathName yra "/"
            }
            else{
                if(strlen(&link[pathStartPosition]) < maxPathLength){
                    strcpy(pathName, &link[pathStartPosition]);
                }
                else{
                    strncpy(pathName, &link[pathStartPosition], maxPathLength);
                    pathName[maxPathLength - 1] = '\0';
                }
                isPathFound = true;
            }
        }
    }
    else{  //Jei domenas nenurodytas linke
        if(strlen(link) < maxPathLength - 1){ //-1, nes gali tekt pridet simboli '/' pradzioje
            strcpy(&pathName[((link[0] == '/') ? 0 : 1)], link);
        }
        else{
            strncpy(&pathName[((link[0] == '/') ? 0 : 1)], link, maxPathLength - 1);
            pathName[maxPathLength - 1] = '\0';
        }
        isPathFound = true;
    }
    return isPathFound;
}

void WebSite::fooDownload(char link[]){
    #define PACKET_SIZE    1024
    string query;
    char protocolName[MAX_PROTOCOL_NAME_LENGTH];
    char domainName[MAX_LINK_LENGTH];
    char pathName[MAX_LINK_LENGTH];
    char buffer[1024 + 1]; //PACKET_SIZE + 1 (null terminator)
    int packetSize = 1024;//PACKET_SIZE
    int64_t time, dtime;
    signed int downloadedPerIterration = 1, downloadedPerFor;
    unsigned long int downloaded = 0;
    int forIterrations = 1;
    bool firstTime = true;
    bool isLocationChecked = false;
    unsigned long int contentLength = 0;
    uint64_t x;
    bool isDomainNameInLink;
    bool isProtocolNameInLink;

    string prevSiteName(CurrentSiteName);
    string downloadSiteName;

    //bool wasOldConnectionSecure = isSecureConnection;

    #ifdef DEBUG
    log << "  ---> PRASIDEJO fooDownload() <---" << endLogRec;
    #endif

    isDomainNameInLink = extractDomainName(link, domainName, MAX_LINK_LENGTH);
    isProtocolNameInLink = extractProtocolName(link, protocolName, MAX_PROTOCOL_NAME_LENGTH);
    extractPathName(link, pathName, MAX_LINK_LENGTH);

    ///Prerelease 20140820
    if(isProtocolNameInLink){
        downloadSiteName = protocolName;
        downloadSiteName += "://";
        downloadSiteName += domainName;
    }
    else{
        downloadSiteName = domainName;
    }
    ///----------------------

    query  = "GET ";
    ///Pvz.: query += "/bt/6740f0d7bdf863d2d9cdf4e40be89eae206831ad/data/NOOBS_v1_3_9.zip";//link;
    query += pathName;
    query += " HTTP/1.1\r\n";
    query += "Host: ";
    ///Pvz.: query += "212.187.212.42\r\n"; //domainName;
    query += (isDomainNameInLink ? domainName : Name.c_str());
    query += "\r\n";
    query += "Connection: close\r\n"; //Keep-Alive\r\n";

    query += "Accept: text/html, application/xhtml+xml, */*\r\n";
    query += "User-Agent: ";
    query += this->UserAgent;
    query += "\r\n";
    ///Pvz.: query += "Referer: http://vija.awardspace.com/\r\n";
    query += "Referer: ";
    query += protocolName;
    query += "://";
    query += domainName;
    query += "\r\n";
    query += "Accept-Encoding: gzip, deflate\r\n";
    query += "Accept-Language: en-US\r\n";
    ///Pvz.: query += "DNT: 1\r\n" (nenaudoju);
    query += "\r\n";

    time = GetTimeMs64();

    if(CurrentSiteName != downloadSiteName){ ///Fragmentas padetas po if() 20140828, kad pagreitinti darba
        sc.reconnect(); /// pakeistas siom eilutem: (0808). 0828 atkeistas
        ///sc.EndConnection();
        ///if(strcmp(protocolName, "https")){
        ///    isSecureConnection = true;
        ///}
        ///else isSecureConnection = false; ///0828

        log << "Naujas: " << downloadSiteName << "  Senas: " << prevSiteName << endLogRec;
        if(!SetName((char*)downloadSiteName.c_str())){
            #ifdef DEBUG
            log << "ERROR: Nesekmingas SetName((char*)downloadSiteName.c_str()" << endLogRec;
            #endif
            return; //Nepavyko prisijungti. Tarkim, parsiunte.
        }
        #ifdef DEBUG
        log << "Sekmingai prisijungta prie naujo domeno. Pradedamas siuntimas..." <<  endLogRec;
        #endif
        CurrentSiteName = downloadSiteName;
    }
    BIO_puts(sc.bio, query.c_str());
    #ifdef DEBUG
    log << "fooDownload: " << endLogRec << query << endLogRec;
    #endif

    logData.myPort = sc.GetIPandPort(logData.myIP, true);
    logData.srvPort = sc.GetIPandPort(logData.srvIP, false);
    logData.bytesSent = query.length();
    logData.link = link;

    /* Priimu atsakyma */
    do{
        downloadedPerIterration = 0;
        time = GetTimeMs64();
        for(x = 0, downloadedPerFor = 0; (x < forIterrations); x++){
            int sek = 1;
            if((contentLength == 0) || firstTime || (downloaded < contentLength)){
                downloadedPerIterration = sc.getAnswerFragment(buffer, packetSize);
                ///log << "getAnswerFragment(): gauta baitu: " << downloadedPerIterration << endLogRec;
                if (downloadedPerIterration < 0) return; //Jei ivyko klaida
///--------------------------[ Jei yra nukreipimas su Location ]---------------------------
                if (!isLocationChecked){
                    char *location;
                    char redirect[MAX_LINK_LENGTH];
                    char domain[MAX_DOMAIN_NAME_LENGTH];
                    buffer[downloadedPerIterration] = '\0';
                    location = strstr(buffer, "\r\nLocation: "); //20140829
                    #ifdef DEBUG
                    ///log << "Po strstr()." << endLogRec;
                    #endif
                    if(location){
                        int endPos;
                        location += 2; //CR-LF
                        location += sizeof("Location: ") - 1;
                        for(endPos = 0; (location[endPos] != '\r') && (location[endPos] != '\n') && endPos < PACKET_SIZE; endPos++); ///
                        if(endPos < PACKET_SIZE){ //Apsaugine salyga
                            location[endPos] = '\0';
                            strcpy(redirect, location);
                            #ifdef DEBUG
                            log << " ---> NUKREIPIMAS I " << redirect << " <--- " << endLogRec;
                            #endif
                            if(extractDomainName(redirect, domain, MAX_DOMAIN_NAME_LENGTH)){
                                char stackCheck; //Galimo steko perpildymo patikrinimui
                                WebSite *ws = new WebSite;
                                // Galimo steko perpildymo del rekursijos tikrinimas (gali sukelti klaida sioje programoje arba klaidingas serverio atsakymas
                                // Compare addresses of stack frames
                                if (g_firstStack == NULL){
                                    g_firstStack = &stackCheck;
                                }
                                if (g_firstStack > &stackCheck  &&  g_firstStack - &stackCheck > MAX_STACK  ||
                                    &stackCheck > g_firstStack  &&  &stackCheck - g_firstStack > MAX_STACK){
                                    printf("Stack is larger than %lu\n", (unsigned long)MAX_STACK);
                                    log << "Gresia steko perpildymas del besikartojanciu nukreipimu sukeliamo metodo \"WebSite\" rekursijos ";
                                    log << "(programos ar serverio klaida)!!! Stekas didesnis uz " << MAX_STACK << endLogRec;
                                    log << "Todel rekursija uzbaigiama." << endLogRec;
                                }
                                else{
                                    ws->SetName(domain);
                                    ws->fooDownload(redirect);
                                }
                                delete ws;
                            }
                            #ifdef DEBUG
                            log << " ---> GRIZO IS NUKREIPIMO <--- " << endLogRec;
                            #endif
                            return; //Tikrai yra nukreipimas - funkcijos vykdyti toliau nera prasmes
                        }
                    }
                    isLocationChecked = true;
                }
///----------------------------------------------------------------------------------------
                if(downloadedPerIterration == 0) break;
                downloaded += downloadedPerIterration;
                downloadedPerFor += downloadedPerIterration; //downloadedPerFor - kiek buvo parsiusta duomenu per visa FOR ciklo vykdyma.
                if(contentLength && ((contentLength - downloaded) / PACKET_SIZE == 0)){
                    ////////////////////////////////// packetSize = (contentLength - downloaded) % PACKET_SIZE;
                }
            }
            if(firstTime){
                buffer[1000] = '\0';
                contentLength = extractContentLength(buffer);
                #ifdef DEBUG
                log << endLogRec << "Atsakymo pradzia: " << endLogRec << buffer << endLogRec;
                log << "Failo dydis: " << contentLength << endLogRec;
                #endif
                firstTime = false;
            }

        }
//log << "fooDownload, po for()" << endLogRec;
        dtime = GetTimeMs64() - time;
        /* Kad isvengti DIV 0 skaiciuojant greiti */
        if(dtime / 1000 <= 0){
           forIterrations *= 2;
           dtime = 1;
        }
        else if(dtime / 1000 > 10){
           forIterrations /= 2;
        }
        else{
            unsigned long int progress = (float)(downloaded) * 100 / ((float)contentLength);
            //cout << "Uztr: " << dtime << "ms  Pars: " << downloaded / 1024 << " KB is " << (contentLength) / 1024
            //     << " KB  Greitis: "  << (((float)downloadedPerFor/1024)/((float)dtime/1000)) << " KB/s  Progr: " << progress << "%\r\n";
            log << "Uztr: " << dtime << "ms  Pars: " << downloaded / 1024 << " KB is " << (contentLength) / 1024
                << " KB  Greitis: "  << (((float)downloadedPerFor/1024)/((float)dtime/1000)) << " KB/s  Progr: " << progress << "%" << endLogRec;
        }
    }
    while(downloadedPerIterration > 0);
    //cout << "fooDownload baige" <<endl;

    /* Jeigu parsisiunciamas failas yra kitame domene, tai reikejo prisijungti prie  kito. Dabar atsijungiama ir jungiamasi prie senojo */
    if(prevSiteName != CurrentSiteName){ //Arba galima rasyti ir taip: (OldSiteName != downloadSiteName)
        if(SetName((char*)prevSiteName.c_str())){
            log << "Domenas po parsisiuntimo sekmingai atkeistas" << endLogRec;
        }
        else{
            log << "Nesekmingas domeno atkeitimas po failo parsisiuntimo" << endLogRec;
        }
    }
    #ifdef DEBUG
    log << endLogRec << "Atkeistas (jei reikejo): " << CurrentSiteName << " Parsisiuntimo: " << downloadSiteName << endLogRec;
    log << "  ---> BAIGESI fooDownload() <---" << endLogRec;
    #endif
    logData.bytesRecv = downloaded;
    writeLogDataLine(&logData, &g_TSV_file);
}

int64_t GetTimeMs64(){
     // Saltinis: http://stackoverflow.com/questions/1861294/how-to-calculate-execution-time-of-a-code-snippet-in-c
     /* Windows */
     FILETIME ft;
     LARGE_INTEGER li;

     /* Get the amount of 100 nano seconds intervals elapsed since January 1, 1601 (UTC) and copy it
      * to a LARGE_INTEGER structure. */
     GetSystemTimeAsFileTime(&ft);
     li.LowPart = ft.dwLowDateTime;
     li.HighPart = ft.dwHighDateTime;

     uint64_t ret = li.QuadPart;
     ret -= 116444736000000000LL; /* Convert from file time to UNIX epoch time. */
     ret /= 10000; /* From 100 nano seconds (10^-7) to 1 millisecond (10^-3) intervals */

     return ret;
}

unsigned long int WebSite::extractContentLength(char *httpAnswerHeader){
    char *clStart;

    if(clStart = strstr(httpAnswerHeader, "Content-Length: ")){
        clStart += sizeof("Content-Length: ") - 1;
        for(int x = 0; strlen(clStart) > x; x++){
            if (clStart[x] == '\r' || clStart[x] == '\n'){ //Gali buti \r\n, bet gali buti ir \n\r
                clStart[x] = '\0';
                break;
            }
        }
        return atol(clStart);
    }
    return 0;
}
///
void WebSite::fooLoadFiles(char currentDir[], char page[], unsigned long int pageLength, char tag[], char attribute[], char *whenAttribute, char *whenValue){
    char currentDomainName[MAX_DOMAIN_NAME_LENGTH];
    unsigned long int curPosition = 0, oldPosition = 0;
    char fileLink[MAX_LINK_LENGTH];
    unsigned long int curLinkNr = 0;
    char fileDomainName[MAX_DOMAIN_NAME_LENGTH];
    char protocol[5];
    char whenGotValue[MAX_ATTRIB_VAL]; //define!
    bool isWhenEqual;
    string Cache[MAX_CACHED_FILES];
    string CacheExt[MAX_CACHED_FILES];
    int nrFilesInCache = 0;
    int cachePosition = 0;
    int nrFilesInCacheExt = 0;
    int cachePositionExt = 0;
    int i;
    string strFileLink, strFileLinkExt;
    string fullPath; //Pvz.: http://www.afaustas.lt/ana/

    #ifdef DEBUG
    log << "fooLoadFiles(): " << endLogRec;
    #endif

    extractDomainName(currentDir, currentDomainName, MAX_DOMAIN_NAME_LENGTH);

    do{
        isWhenEqual = false;
        if(whenAttribute != NULL) {
            oldPosition = curPosition;
            curPosition = TagParser(tag, whenAttribute, whenGotValue, MAX_ATTRIB_VAL, page, pageLength, curPosition);
            if(curPosition >= sc.GetLengthOfAnswer()){
                break;
            }
            if(!memicmp(whenValue, whenGotValue, strlen(whenValue))){
                isWhenEqual = true;
                curPosition = oldPosition;
            }
        }
        if(isWhenEqual || (whenAttribute == NULL)){
            bool domenasYra;

            curPosition = TagParser(tag, attribute, fileLink, MAX_LINK_LENGTH, page, pageLength, curPosition);
            if(curPosition < sc.GetLengthOfAnswer()){ ///08/05 -pridetas sis if(){}
                #ifdef DEBUG
                log << "File link: " << fileLink << endLogRec;
                #endif
                domenasYra = extractDomainName(fileLink, fileDomainName, MAX_DOMAIN_NAME_LENGTH);
                #ifdef DEBUG
                log << " DOMENAS( TAGE:   " << fileDomainName << "  DABARTINIS: " << currentDomainName << " )" << endLogRec;
                #endif
                if(strcmp(fileDomainName, currentDomainName) && domenasYra){
                    //Tag'e nurodytas kitas domenas
                    strFileLinkExt = fileLink;
                    //Primityvus cache'o imitavimas, kad nekartotu daug vienodo paveiksliuko krovimu
                    for(i = 0; (i < MAX_CACHED_FILES) && (CacheExt[i] != strFileLinkExt); i++){
                    //    cout << i << ": " << CacheExt[i] << endl;
                    }
                    if(i == MAX_CACHED_FILES){ //jei nera kese
                        fooDownload(fileLink);
                        CacheExt[cachePositionExt++] = fileLink; //Padedu i kesa
                        if(cachePositionExt == MAX_CACHED_FILES) cachePositionExt = 0;
                    }
                }
                else{
                    //Tag'e nurodytas domenas sutampa su dabartiniu arba is viso nenurodytas
                    ///char protocolName[MAX_PROTOCOL_NAME_LENGTH]; //Uzkom 0828

                    strFileLink = fileLink;
                    ///extractProtocolName(fileLink, protocolName, MAX_PROTOCOL_NAME_LENGTH); Uzkom 0828
                    /*fullPath = "http"; //Palaikomas tik http protokolas
                    fullPath += "://";
                    fullPath += currentDomainName; */
                    fullPath = currentDir;
                    fullPath += extractDirectoryName(fileLink);
                    fullPath += extractFileName(fileLink);
                    #ifdef DEBUG
                    log << endLogRec << " DIR NAME: " << extractDirectoryName(fileLink) << endLogRec;
                    log << "!PILNAS KELIAS: " << fullPath << endLogRec;
                    #endif
                    for(i = 0; (i < MAX_CACHED_FILES) && (Cache[i] != strFileLink); i++){
                        //cout << i << ": " << Cache[i] << endl;
                    }
                    if(i == MAX_CACHED_FILES){ //jei nera kese
                        fooDownload((char*)fullPath.c_str()); //Kese nera, tad reikia siustis
                        Cache[cachePosition++] = fileLink; //Padedu i kesa
                        if(cachePosition == MAX_CACHED_FILES) cachePosition = 0;
                    }
                }
            }
        }
    }
    while(curPosition < sc.GetLengthOfAnswer());
}

void WebSite::Browse(const string webSiteName, unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax){
    unsigned long int time;
    unsigned int depth, curDepth = 0;
    bool foundProtocolData;
    char protocol[MAX_PROTOCOL_NAME_LENGTH];
    char domain[MAX_DOMAIN_NAME_LENGTH];
    char newPath[MAX_LINK_LENGTH] = "";
    string firstPageLink;
    string fullNextLink;
    string originalName = Name; //Name apibreztas pacioje klaseje

    if(!extractProtocolName(webSiteName.c_str(), protocol, MAX_PROTOCOL_NAME_LENGTH)){
        firstPageLink = (isSecureConnection ? ("https://"):("http://"))  + webSiteName;  //Jei protokolas nenurodytas, jis prirasomas
    }
    else if(strcmp(protocol, "http") && strcmp(protocol, "https")){  //Jei nei http, nei https, tai baigiu, nes kiti protokolai nepalaikomi
        return;
    }
    else{
        firstPageLink = webSiteName; //Jeigu ieina ir protokolas, kuris yra palaikomas, ir dar, galbut, kelias
    }
    logData.link = firstPageLink; //I log'a ir tsv rasau link'a

    (depthMin == depthMax)? (depth = depthMin):(depth = (rand() % (depthMax - depthMin)) + depthMin); //Atsitiktinai parenku gyli
    #ifdef DEBUG
    log << "firstPageLink = " << firstPageLink << " newPath=";
    #endif
    if(!LoadPage(firstPageLink.c_str(), newPath, MAX_LINK_LENGTH)){//Uzkraunu pagrindini puslapi ir gaunu dideli sarasa nuorodu
        free(sc.answer);  //sc.answer - cia saugomas atsakymas is serverio i uzklausa GET /
        return; //"Vartotojas" "iveda" kito puslapio adresa
    }
    free(sc.answer); //Linku sarasas gautas, tad sc.answer nebereik
    //if(newPath[0] != '\0'){
        fullNextLink = newPath;
    //}
    #ifdef DEBUG
    log << newPath << endLogRec;
    #endif

    do{  //Ilgai ir (butu gerai jei) laimingai narsau
        int selectedLinkNr;
        char protocol[MAX_PROFILE_LEN] = "";

        /// 20140829 pakeista tolimesnem dviem eilutem selectedLinkNr = LinksInList ? (rand() % LinksInList) : 0;  //Atsitiktinai pasirenku nuoroda is susidaryto saraso
        if(LinksInList == 0) break;
        selectedLinkNr = rand() % LinksInList;  //Atsitiktinai pasirenku nuoroda is susidaryto saraso

        foundProtocolData = extractProtocolName(linkList[selectedLinkNr], protocol, MAX_PROTOCOL_NAME_LENGTH);
        #ifdef DEBUG
        log<<"Pasirinkta nuorod: " << selectedLinkNr << " o tai yra: " << linkList[selectedLinkNr] << endLogRec;
        #endif
        /* Sitame cikle pasalinu nuorodas i to paties puslapio skirt. vietas. Pvz.: http://www.afaustas.lt/an/index.php#enc */
        for(int i = 0; linkList[selectedLinkNr][i] && (i < MAX_LINK_LENGTH); i++){
            if(linkList[selectedLinkNr][i] == '#'){
                linkList[selectedLinkNr][i] = '\0';
            }
        }
//        if((linkList[selectedLinkNr][0] == '#') || (strcmp(protocol, "http") && foundProtocolData)) { ///Hmm, sitas if() po for() nebeturetu, manau, niekad ivykti
//            continue; <--
//        }
        if(strcmp(protocol, "http") && strcmp(protocol, "https") && foundProtocolData) {
            //Nepalaikomas protokolas
            log << "Browse(): Nepalaikomas protokolas" << endLogRec;
            break;
        }

        {
            /* linkas "mailto:" nepalaikoma */
            char tmp = linkList[selectedLinkNr][sizeof("mailto:") - 1];
            linkList[selectedLinkNr][sizeof("mailto:") - 1] = '\0';
            if(!strcmp(linkList[selectedLinkNr], "mailto:")){  //Sutampa
                linkList[selectedLinkNr][sizeof("mailto:") - 1] = tmp;
                ///linkList[selectedLinkNr][0] = 0; //Sekanti karta praleist, kad neuzsiciklintu (N/A) - o gal irasyt adresa i pradzia ar ta pati puslapi?
                break; //Is naujo parinkti atsitiktini linka
            }
            linkList[selectedLinkNr][sizeof("mailto:") - 1] = tmp; //Atstato
            /* "javascript:" nepalaikoma */
            tmp = linkList[selectedLinkNr][sizeof("javascript") - 1];
            linkList[selectedLinkNr][sizeof("javascript") - 1] = '\0';
            if(!strcmp(linkList[selectedLinkNr], "javascript")){  //Sutampa
                linkList[selectedLinkNr][sizeof("javascript") - 1] = tmp;
                ///linkList[selectedLinkNr][0] = 0;
                break;
            }
            linkList[selectedLinkNr][sizeof("javascript") - 1] = tmp;
        }

        /* Sprendziama, ka daryti ivairiais link'o atvejais */
        //Pvz.: http://www.afaustas.lt
        //      http://www.afaustas.lt/zai.html
        //      www.afaustas.lt
        //      www.afaustas.lt/zai.html
        //      /zai.html
        //      zai.html
        if(!extractDomainName(linkList[selectedLinkNr], domain, MAX_DOMAIN_NAME_LENGTH)){ //Nera domeno vardo
            //TODO: Tik prisideda ir prisideda... Prisideda ir grizimas atgal - reiketu padaryti,
            //kad tuomet linkas butu trumpinamas, o ne ilginamas, t. y. kad nebutu tokiu situaciju:
            //http://www.afaustas.lt////////an///../file.html, nors tai ir veikia normaliai.
            if(linkList[selectedLinkNr][0] == '/'){
                size_t pos;
                ///fullNextLink += &linkList[selectedLinkNr][1]; //Cia reiketu grizti i sakni!!! - tad uzkom, ir prided. sek. eilutes 20140828
                pos = fullNextLink.find("://");
                if(pos == string::npos){
                    pos = 0;
                }
                pos += 3;
                pos = fullNextLink.find('/', pos);
                fullNextLink.erase(pos, string::npos);
                fullNextLink += linkList[selectedLinkNr];
            }
            else{
                fullNextLink += linkList[selectedLinkNr];
            }
        }
        else fullNextLink = linkList[selectedLinkNr]; //Yra domeno vardas

        #ifdef DEBUG
        //Galiausia sudaryta pilna nuoroda, t. y. is visu auksciau pateiktu pvz gaunama http://www.afaustas.lt/zai.html
        log << "---> Sudaryta pilna nuoroda: " << fullNextLink << endLogRec;
        #endif

        (timeMin == timeMax)  ? (time = timeMin)  :(time = (rand() % (timeMax - timeMin)) + timeMin); //Atsitiktinumo budu parenkamas laikas
        #ifdef DEBUG
        log << "Gylis dabar: " << curDepth + 1<< " is " << depth << endLogRec;
        log << "Laukta: " << (float)time/1000 << " sekundziu" << endLogRec;
        #endif // DEBUG
        Sleep(time);

        /* Uzkraunamas puslapis */
        if(!LoadPage(fullNextLink.c_str(), newPath, MAX_LINK_LENGTH)){ //Sis metodas keicia linkList[][]
            #ifdef DEBUG
            log << "Linku sarase: " << LinksInList << endLogRec;
            #endif
            //Kai grizta su false, tai kelias turetu grizt i pradini, nes aklaviete - linku sarase nebera.
            continue; ///TODO: Ar vera rasyt sia eilute? - jei linkas tik vienas/keli ir po jo/ju linku nera,tai gali uzsiciklint, manau. Turbut geriausia darba uzbaigti (iseit is metodo) ir gad butu parenkamas kitas tinklapis.
        }
        fullNextLink = newPath; //newPath - pakeista (arba nepakeista) dabartine direktorija

        curDepth++;
        #ifdef DEBUG
        log << "Gylis dabar: " << curDepth << " is " << depth << endLogRec;
        #endif
    }
    while(LinksInList && (curDepth < depth));
    #ifdef DEBUG
    log << "Situacija - LinksInList=" << LinksInList << " curDept=" << curDepth << " depth=" << depth << endLogRec;
    #endif // DEBUG
    SetName((char*)originalName.c_str()); ///SetName argumentas turetu buti const char!!! TODO: <--
}

///Wrapperis Browse() metodui. TODO: Gal pasalinti??
void WebSite::Browse(unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax){
    //cout << extractFileName("http://www.lala.lt/anas/mano/yra/aaa.htm");
    //system("pause");
    Browse(Name, depthMin, depthMax, timeMin, timeMax);
}

const char *WebSite::extractFileName(const char link[]){
    int x;
    for(x = strlen(link) - 2; (link[x] != '/') && x; x--);
    return &link[++x];
}

string WebSite::extractDirectoryName(const char link[]){
    char *domainNameStartPointer;
    int domainNameStartPosition;
    int directoryNameStartPosition;
    int directoryNameEndPosition;
    string dir("");

    /*  Pasitaiko, kad linkai, kuriuose yra nurodytas domenas nebutinai susideda is pradzioje nurodomo protokolo, kaip
        pavyzdziui "http://www.afaustas.lt/an/index.php" . Taciau pastebejau, kad pasitaiko ir tokiu atveju:
        "//s.ytimg.com/yts/jsbin/html5player-lt_LT-vflEkvKvu/html5player.js" . Siuo atveju domenas yra "s.ytimg.com"
        Nors iprastai buna toks domeno isskyrimas: http://www.afaustas.lt/an/index.php ==> "www.afaustas.lt" */
    if(strlen(link)){
        if((strlen(link) > 5) && (link[0] == '/') && (link[1] == '/')){
            char *dirNameStartPositionPtr = strchr(&link[2], '/');
            if(dirNameStartPositionPtr == NULL){ //Jei situacija tokia: //failas.php, tuomet dir="/" (iterpta 20140819)
                return "";
            }
            directoryNameStartPosition = dirNameStartPositionPtr - link;
            domainNameStartPosition = 2;
        }
        else{
            domainNameStartPointer = strstr(link, "://");
            if(domainNameStartPointer){
                domainNameStartPosition = domainNameStartPointer - link;
                domainNameStartPosition += sizeof("://");
                for(directoryNameStartPosition = domainNameStartPosition; link[directoryNameStartPosition] != '/' && link[directoryNameStartPosition]; directoryNameStartPosition++);
            }
            else {
                directoryNameStartPosition = 0;
            }
        }
        for(directoryNameEndPosition = strlen(link) - 1; link[directoryNameEndPosition] != '/' && directoryNameEndPosition; directoryNameEndPosition--);
        if(directoryNameEndPosition) directoryNameEndPosition += 1;
        dir.insert(0, &link[directoryNameStartPosition], directoryNameEndPosition - directoryNameStartPosition);
    }
    return dir;
}

void WebSite::removeNewLinesAndChunkData(char *link){
    char *nl1, *nl2;
    int x, y;
    nl1 = strstr(link, "\r\n");
    if(nl1 != NULL){
        nl2 = strstr(&nl1[1], "\r\n");
        log << "PRIES: " << link << endLogRec;
        if(nl2 != NULL){
            //Siuo atveju per viduri linke yra skaiciukas, nurodantis sekancio chunk dydi (perdavimas HTTP chunked)
            nl2+=2;
            for(x = 0; nl2[x]; x++){
                nl1[x] = nl2[x];
            }
            nl1[x] = '\0';
        }
        log << "PO: " << link << endLogRec;
    //    system("pause");
    }

    x = y = 0;
    while(link[y]){
        link[x] = link[y++];
        if((link[x] != '\n')&&(link[x] != '\r')){
            x++;
        }
    }
    link[x] = '\0';
}
