﻿# UInternetAGen

Real traffic generator for Windows. Simulates user daily activity on the web (browsing various websites, searching on Google, watching Youtube, sending and receiving emails (only IMAP supported)). Supports SSL. This program has been developed for research purposes. Currently available only in Lithuanian language.

**Discontinued in September 2014** and uploaded in 2019. I didn't use any version control system during its development so there is no commit history. I decided to upload it here because I hope this will be useful for somebody. In case somebody will be very interested perhaps I could continue working on it during my free time.

This project can be opened using CodeBlocks.

Program has been tested under Windows XP and Windows 7.

LT: Kompiuterių tinklo realaus srauto generatorius

You can download binary here: https://drive.google.com/file/d/1m0TUKo02W7TSozhkZRCZC0qyWKZmvEDX/view?usp=sharing .
 
## Screenshots
 
 ![Main window](/screenshots/main-window.png?raw=true "Main Window")
 
 ![e-mail settings dialog](/screenshots/email-settings.png?raw=true "e-mail settings")
 
 ![About dialog](/screenshots/about.png?raw=true "About dialog")
