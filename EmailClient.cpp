#include "EmailClient.h"

/* Base64 encoding funkcijai */
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
/* END: Base64 encoding funkcijai */

extern logger log;

/* SITA FUNKCIJA KOLKAS YRA CHALTURA. KEISTI!!! */
/* Uzdaviau klausima: http://stackoverflow.com/questions/25516174/imap-client-using-openssl-library */
bool checkEmail(const char *server, unsigned int port, bool useSSL,
                const char *user, const char *password, unsigned int listTime, unsigned int showTime){
    char tmp[1000];
    string serverWithPort, query;
    char portAsChar[10];
    int bytesRead = 0;
    SocketCommunication sc;
    LOGDATA logData;
    extern fstream g_TSV_file;

    itoa(port, portAsChar, 10);
    serverWithPort = server;
    serverWithPort += ":";
    serverWithPort += portAsChar;

    cout << "--DOM: " << serverWithPort << " ath: " << useSSL << endl;
    if(!sc.SetConnection((char*)serverWithPort.c_str(), useSSL)){
        log << "Nepavyko prisijungti prie IMAP serverio" << endLogRec;
        return false;
    }
    logData.link = "imap://";
    logData.link += serverWithPort;

    logData.myPort = sc.GetIPandPort(logData.myIP, true);
    logData.srvPort = sc.GetIPandPort(logData.srvIP, false);

    log << "Pradedama IMAP sesija..." << endLogRec;
    memset(tmp, 0, sizeof(tmp));
    logData.bytesRecv = BIO_read(sc.bio, tmp, 1000); // Receive server welcome string
    logData.bytesSent = 0;
    log << "Server welcome message: " << tmp << endLogRec;
    writeLogDataLine(&logData, &g_TSV_file);

    log << "LOGIN: ";
    memset(tmp, 0, sizeof(tmp));
    query = "tag LOGIN ";
    query += user;
    query += " ";
    query += password;
    query += "\r\n";
    logData.bytesSent = BIO_puts(sc.bio, query.c_str());
    /*BIO_puts(sc.bio, user);
    BIO_puts(sc.bio, " ");
    BIO_puts(sc.bio, password);
    BIO_puts(sc.bio, "\r\n");*/
    logData.bytesRecv = BIO_read(sc.bio, tmp, sizeof(tmp) - 1);
    log << tmp << endLogRec;
    writeLogDataLine(&logData, &g_TSV_file);

    log << "LIST: ";
    memset(tmp, 0, sizeof(tmp));
    logData.bytesSent = BIO_puts(sc.bio, "tag LIST \"\" \"*\"\r\n");
    logData.bytesRecv = BIO_read(sc.bio, tmp, sizeof(tmp) - 1);
    writeLogDataLine(&logData, &g_TSV_file);
    log << tmp << endLogRec;

    Sleep(listTime * 1000);

    log <<"INBOX: " << endLogRec;
    memset(tmp, 0, sizeof(tmp));
    logData.bytesSent = BIO_puts(sc.bio, "tag select INBOX\r\n");
    logData.bytesRecv = BIO_read(sc.bio, tmp, sizeof(tmp) - 1);
    writeLogDataLine(&logData, &g_TSV_file);
    log << tmp << endLogRec;

    log << "STATUS: " << endLogRec;
    memset(tmp, 0, sizeof(tmp));
    logData.bytesSent = BIO_puts(sc.bio, "tag STATUS INBOX (MESSAGES)\r\n");
    logData.bytesRecv = BIO_read(sc.bio, tmp, sizeof(tmp) - 1);
    writeLogDataLine(&logData, &g_TSV_file);
    log << tmp << endLogRec;

    ///todo: atidaryti kazkuri laiska ir panaudoti showTime

    log << "LOGOUT: " << endLogRec;
    memset(tmp, 0, sizeof(tmp));
    logData.bytesSent = BIO_puts(sc.bio, "tag LOGOUT\r\n");
    logData.bytesRecv = BIO_read(sc.bio, tmp, sizeof(tmp) - 1);
    writeLogDataLine(&logData, &g_TSV_file);
    log << tmp << endLogRec;

    sc.EndConnection();

    return true;
}

bool sendEmailMessage(const char *server, unsigned int port, bool useSSL,
                      const char *user, const char *password,
                      const char *from, const char *to, const char *subject, const char *message){

    LOGDATA logData;
    SOCKET s; //Nereikalinga
    SocketCommunication sc;
    string query;
    char answer[MAX_SMTP_ANSWER_LENGTH];
    string serverWithPort;
    char portAsChar[10];
    char *credentials;
    char *encodedCredentials;
    int bytesRead;
    extern fstream g_TSV_file;

    itoa(port, portAsChar, 10);
    serverWithPort = server;
    serverWithPort += ":";
    serverWithPort += portAsChar;

    cout << "--DOM: " << serverWithPort << " ath: " << useSSL << endl;
    if(!sc.SetConnection((char*)serverWithPort.c_str(), useSSL)){
        log << "Nepavyko prisijungti prie SMTP serverio" << endLogRec;
        return false;
    }

    logData.link = "smtp://";
    logData.link += serverWithPort;

    //memset((LOGDATA*)&logData, 0, sizeof(logData)); ///-- LUZTA DEL NEAISKIU PRIEZASCIU TODO: <--
    logData.myPort = sc.GetIPandPort(logData.myIP, true);
    logData.srvPort = sc.GetIPandPort(logData.srvIP, false);

    memset(answer, 0, sizeof(answer));
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error; //Ar negali buti taip, kad grazins ne visa atsakyma? Nors neturetu, nes trumpas labai.

    logData.bytesRecv = bytesRead;
    logData.bytesSent = 0;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '2' && answer[1] == '2' && answer[2] == '0')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    logData.bytesRecv = 0;
    memset(answer, 0, sizeof(answer));
    query = "EHLO ";
    query += server;
    query += "\r\n";
    logData.bytesSent = BIO_puts(sc.bio, query.c_str());
    log << "SMTP uzklausa: " << query << endLogRec;
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error;
    logData.bytesRecv = bytesRead;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '2' && answer[1] == '5' && answer[2] == '0')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    logData.bytesRecv = 0;
    logData.bytesSent = 0;


    if(user != NULL){
        credentials = new char [strlen(user) + strlen(password) + 4];
        memset(credentials, 0, strlen(user) + strlen(password) + 4);
        strcpy(&credentials[1], user);
        strcpy(&credentials[1 + strlen(user) + 1], password);

        memset(answer, 0, sizeof(answer));
        query = "AUTH PLAIN ";
        size_t output_length;
        encodedCredentials = base64_encode((const unsigned char *)credentials, 1 + strlen(user) + 1 + strlen(password), &output_length);
        encodedCredentials[output_length] = '\0';
        if(encodedCredentials == NULL) goto SEMsg_error;
        query += encodedCredentials;
        free(encodedCredentials);
        delete[] credentials;
        query += "\r\n";
        logData.bytesSent = BIO_puts(sc.bio, query.c_str());
        log << "SMTP uzklausa: " << query << endLogRec;
        bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
        if(!bytesRead) goto SEMsg_error;
        logData.bytesRecv = bytesRead;
        log << "SMTP atsakymas: " << answer << endLogRec;
        if(!(answer[0] == '2' && answer[1] == '3' && answer[2] == '5')) goto SEMsg_error;
    }

    memset(answer, 0, sizeof(answer));
    query = "MAIL FROM:<";
    query += from;
    query += ">\r\n";
    logData.bytesSent = BIO_puts(sc.bio, query.c_str());
    log << "SMTP uzklausa: " << query << endLogRec;
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error;
    logData.bytesRecv = bytesRead;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '2' && answer[1] == '5' && answer[2] == '0')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    memset(answer, 0, sizeof(answer));
    query = "RCPT TO:<";
    query += to;
    query += ">\r\n";
    logData.bytesSent = BIO_puts(sc.bio, query.c_str());
    log << "SMTP uzklausa: " << query << endLogRec;
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error;
    logData.bytesRecv = bytesRead;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '2' && answer[1] == '5' && answer[2] == '0')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    memset(answer, 0, sizeof(answer));
    query = "DATA\r\n";
    BIO_puts(sc.bio, query.c_str());
    log << "SMTP uzklausa: " << query << endLogRec;
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error;
    logData.bytesRecv = bytesRead;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '3' && answer[1] == '5' && answer[2] == '4')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    memset(answer, 0, sizeof(answer));
    query = "Subject: ";
    query += subject;
    query += "\r\n\r\n";
    query += message;
    query += "\r\n\r\n.\r\n";
    logData.bytesSent = BIO_puts(sc.bio, query.c_str());
    log << "SMTP uzklausa: " << query << endLogRec;
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error;
    logData.bytesRecv = bytesRead;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '2' && answer[1] == '5' && answer[2] == '0')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    memset(answer, 0, sizeof(answer));
    query = "QUIT\r\n";
    logData.bytesSent = BIO_puts(sc.bio, query.c_str());
    log << "SMTP uzklausa: " << query << endLogRec;
    bytesRead = BIO_read(sc.bio, answer, sizeof(answer) - 1);
    if(!bytesRead) goto SEMsg_error;
    logData.bytesRecv = bytesRead;
    log << "SMTP atsakymas: " << answer << endLogRec;
    if(!(answer[0] == '2' && answer[1] == '2' && answer[2] == '1')) goto SEMsg_error;
    writeLogDataLine(&logData, &g_TSV_file);

    sc.EndConnection();

    return true;

SEMsg_error:
    log << "Ivyko klaida." << endLogRec;
    writeLogDataLine(&logData, &g_TSV_file);
    sc.EndConnection();
    return false;

}

//Saltinis: http://stackoverflow.com/questions/342409/how-do-i-base64-encode-decode-in-c
char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = (char *)malloc(*output_length);
    int i, j;

    if (encoded_data == NULL) return NULL;

    for (i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}
