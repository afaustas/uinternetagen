//2014-07-31

#include "WebSite.h"
#include <fstream>

#define UNTIL_END_OF_VIDEO    0

class YouTube{
public:
    YouTube(){ws.SetName("https://www.youtube.com");};
    void SetUserAgent(string UserAgent){ws.SetUserAgent(UserAgent);};
    bool LoadMainPage();
    void WatchRandomVideoFromLink();

    //void SearchFor(string keywords); //Rezultatas yra vienas ir padedamas i linku sarasa, o kiti linkai pasalinami

    //void Browse(string keywordsList[], unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax = UNTIL_END_OF_VIDEO);
    //void WatchRandomVideoFromLink();


    //void ChooseBestResult();
    //void Watch();
private:
    WebSite ws; //("https://www.youtube.com");
    char newPath[MAX_LINK_LENGTH];
    void ParseCurrentVideoURL();
    void PercentDecode(char url[]);
    char videoUrl[8192];
};
