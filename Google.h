#include "WebSite.h"

class Google{
public:
    Google(){ws.SetName("https://www.google.lt");};
    bool LoadMainPage();
    void search(string keywords, unsigned int depthMin, unsigned int depthMax, long int timeMin, unsigned long int timeMax);
    void SetUserAgent(string UserAgent){ws.SetUserAgent(UserAgent);};
private:
    WebSite ws;
    char newPath[MAX_LINK_LENGTH];
};
