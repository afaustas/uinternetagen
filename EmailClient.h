#ifndef __EMAILCLIENT_H
#define __EMAILCLIENT_H

#include "socket_level_communication.h"
#include "logger.h"

#include <iostream>  //Sitas tik laikinai
#include <string>
using namespace std;

#define MAX_SMTP_ANSWER_LENGTH  1000

bool checkEmail(const char *server, unsigned int port, bool useSSL,
                const char *user, const char *password, unsigned int listTime, unsigned int showTime);

bool sendEmailMessage(const char *server, unsigned int port, bool useSSL,
                      const char *user, const char *password,
                      const char *from, const char *to, const char *subject, const char *message);

char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length);

#endif // __EMAILCLIENT_H
