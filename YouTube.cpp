#include "YouTube.h"

bool YouTube::LoadMainPage(){
    if(!ws.LoadPage("https://www.youtube.com", newPath, MAX_LINK_LENGTH)){
       return false;
    }
    cout << MAX_LINK_LENGTH;
    //system("pause");
    //cout << "REZ: " << ws.GetAnswer();
    return true;
}

void YouTube::WatchRandomVideoFromLink(){
    int retryies = 50;
    int selectedLink = 0;

    //Parinkineju video is nuorodu saraso (video linkas turi prasideti zodziu watch
    do{
        selectedLink = rand() % ws.LinksInList;
        //cout << ws.linkList[selectedLink] << " " << retryies << endl;
    }
    while(strncmp(ws.linkList[selectedLink], "/watch?", sizeof("watch?")) && --retryies);
    if(!retryies){
        cout << endl << "Sakes! Kazkas netaip." << endl;
        return;
    }
    cout << "Gautas link: " << ws.linkList[selectedLink] << endl;

    //Uzkraunu puslapi su video
    if(!ws.LoadPage(ws.linkList[selectedLink], newPath, MAX_LINK_LENGTH)){
        return;
    }
    cout << "Puslapis uzkrautas" << endl;

    ParseCurrentVideoURL();

    cout << "URL: " << videoUrl << endl;

    ws.fooDownload(videoUrl);
}

void YouTube::ParseCurrentVideoURL(){
    size_t positionStart, positionEnd, currentPosition, currentEnd;
    string page = ws.GetAnswer();
    char *url = videoUrl; //sutrumpinimas kintamojo pavadinimo
    //ofstream f("urls.txt");

    positionStart = page.find("<div id=\"player-api\" class=\"");
    positionStart = page.find("<script>", positionStart);
    positionEnd = page.find("</div>", positionStart);
    currentPosition = positionStart;

    if((positionStart != string::npos) && (positionEnd != string::npos)){
        do{
            currentPosition = page.find("\\u0026url", currentPosition);
            if(currentPosition == string::npos) break;
            currentPosition +=   sizeof("\\u0026url") - 1;
            currentEnd = page.find("\\u0026", currentPosition);
            if(currentEnd - currentPosition < 8192){
                size_t len;
                len = page.copy(url, currentEnd - currentPosition - 1, currentPosition + 1);
                url[len] = '\0';
            }
            currentPosition = currentEnd;
            PercentDecode(url);
            /*{
                cout << url << endl;
                f << url << endl;
            }*/
            if(strstr(url, "---")) break; //URL su domenu, kurio varde yra "---" linkai tinkamiausi
        }
        while(currentPosition < positionEnd);
    }
    //f.close();
}

void YouTube::PercentDecode(char url[]){
    int pos = 0, posProcessed = 0;
    int len = strlen(url);
    char hexByte[3], chr;
    char *urlProcessed = new char[strlen(url + 1)];
    hexByte[2] = '\0';

    /* 1 zingsnis: dekoduoju simbolius */
    for(pos = 0; pos < len; pos++){
        if(url[pos] == '%'){
            hexByte[0] = url[pos + 1];
            hexByte[1] = url[pos + 2];
            chr = (char)strtol(hexByte, NULL, 16);
            url[pos] = chr;
            url[pos + 1] = ' ';
            url[pos + 2] = ' ';
        }
    }

    /* 2 zingsnis: pasalinu tarpus */
    pos = 0;
    while (url[pos] != '\0'){
        if (url[pos] != ' ') {
            url[posProcessed] = url[pos];
            posProcessed++;
        }
        pos++;
    }
    url[posProcessed] = '\0';
    ///cout << url << endl;
}
